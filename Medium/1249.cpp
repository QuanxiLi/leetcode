class Solution {
public:
    string minRemoveToMakeValid(string s) {
        string result;
        int valid_bra = 0, exket = 0;
        int balance = 0;
        int bra_count = 0;
        for(auto c : s) {
            if(c == '(') {
                bra_count++;
                balance++;
            } else if(c == ')') {
                if(balance > 0) {
                    balance--;
                } else {
                    exket++;
                }
            }
        }
        valid_bra = bra_count - balance;
        result.reserve(s.size() - balance - exket);
        for(auto c : s) {
            if(c == '(') {
                if(valid_bra > 0) {
                    valid_bra--;
                } else {
                    continue;
                }
            } else if(c == ')') {
                if(exket > 0) {
                    exket--;
                    continue;
                }
            }
            result.push_back(c);
        }
        return result;
    }
};
