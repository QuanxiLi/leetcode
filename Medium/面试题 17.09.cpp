#include <vector>
#include <cmath>
#include <climits>

static const double log3 = std::log(3);
static const double log5 = std::log(5);
static const double log7 = std::log(7);
static std::vector<int> saved_pow_of_3{1}, saved_pow_of_5{1}, saved_pow_of_7{1};

class Solution {
public:
    inline static int powOf(int n, int base, std::vector<int> &saved) {
        if(saved.size() > n + 1) {
            return saved[n];
        }
        while(saved.size() <= n) {
            if(INT_MAX < static_cast<double>(saved.back()) * base) {
                saved.push_back(INT_MAX);
            } else {
                saved.push_back(saved.back() * base);
            }
        }
        return saved.back();
    }

    inline static int powOf3(int n) {
        return powOf(n, 3, saved_pow_of_3);
    }
    inline static int powOf5(int n) {
        return powOf(n, 5, saved_pow_of_5);
    }
    inline static int powOf7(int n) {
        return powOf(n, 7, saved_pow_of_7);
    }

    // 3^a 5^b 7^* <= bound
    inline static int getNumWithUpperBound(int bound, int b, int c) {
        int three = bound / powOf5(b) / powOf7(c);
        if(three == 0)  return 0;
        int num = std::log(three) / log3;
        int p3n = powOf3(num);
        if(three < p3n)
            return num;
        if(three / 3 >= p3n)
            return num + 2;
        return num + 1;
    }

    inline static int getNumWithUpperBound(int bound, int c) {
        int five = bound / powOf7(c);
        if(five <= 0)
            return 0;
        int num = std::log(five) / log5;
        int p5n = powOf5(num);
        if(five < p5n)
            num--;
        if(five / 5 >= p5n)
            num++;
        int total = 0;
        for(int b = 0; b <= num; b++) {
            total += getNumWithUpperBound(bound, b, c);
        }
        return total;
    }

    inline static int getNumWithUpperBound(int bound) {
        if(bound <= 0)
            return 0;
        int num = std::log(bound) / log7;
        int p7n = powOf7(num);
        if(bound < p7n)
            num--;
        if(bound / 7 >= p7n)
            num++;
        int total = 0;
        for(int c = 0; c <= num; c++) {
            total += getNumWithUpperBound(bound, c);
        }
        return total;
    }

    inline int guess(int k) {
        if(k < 100) {
            return std::pow(10, std::log(k) / std::log(2) - 2);
        } else {
            double v = std::pow(10, k / 100.0 + 4);
            if(v > INT_MAX) {
                return INT_MAX;
            }
            return v;
        }
    }

    int getKthMagicNumber(int k) {
        const int simple_results[] = {0, 1, 3, 5, 7, 9, 15, 21};
        if(k <= 7) {
            return simple_results[k];
        }
        // guess a value
        int bound = guess(k);
        int num = getNumWithUpperBound(bound);
        int up, low;
        int nup, nlow;
        int dn = k - num;
        // get a bound
        if(dn > 0) {
            up = bound;
            nup = num;
            while(dn > 0) {
                low = up;
                nlow = nup;
                up *= 1.5;
                nup = getNumWithUpperBound(up);
                dn = k - nup;
            }
        } else if(dn <= 0) {
            low = bound;
            nlow = num;
            while(dn <= 0) {
                up = low;
                nup = nlow;
                low /= 1.5;
                nlow = getNumWithUpperBound(low);
                dn = k - nlow; 
            }
        }
        // binary search
        while(up - 1 > low) {
            int mid = ((unsigned)up + (unsigned)low) / 2;
            int nmid = getNumWithUpperBound(mid);
            if(nmid >= k) {
                up = mid;
            } else {
                low = mid;
            }
        }
        return up;
    }
};
