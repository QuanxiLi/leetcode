class Solution {
public:
    int numTrees(int n) {
        // numsTrees(n) = T_n
        // T_0 = 0, T_1=0
        // T_n = \sum_{i=0}^{n-1} T_i * T_{n-i-1}
        long long c = 1;
        int np1 = n + 1;
        for(int i = 2, j = 2; i <= np1; i++, j+=4) {
            c = c * j / i;
        }
        return c;
    }
};
