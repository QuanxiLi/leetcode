#include <vector>
#include <algorithm>
#include <cstring>

using namespace std;

class Solution {
public:
    int change(int amount, vector<int>& coins) {
        if(coins.empty()) {
            return amount == 0 ? 1 : 0;
        }
        // change(a, n) = sum{k, change(a-k*coins[n-1], n-1)}
        amount++;
        unsigned *c = new unsigned[amount];
        int coin0 = coins[0];
        memset(c, 0, amount * sizeof(int));
        for(int i = 0; i < amount; i += coin0) {
            c[i] = 1;
        }
        for(int i = 1; i < coins.size(); i++) {
            int coini = coins[i];
            for(int j = coini; j < amount; j++) {
                c[j] = (c[j] + c[j - coini]) & 0x7FFFFFFF;
            }
        }
        int result = c[amount - 1];
        delete [] c;
        return result;
    }
};
