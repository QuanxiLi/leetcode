#include <vector>

using namespace std;

class Solution {
public:
    vector<vector<int>> subsets(vector<int>& nums) {
        typedef vector<int>::size_type size_type;
        size_type size = nums.size();
        size_type max = ~(~static_cast<size_type>(0) << size);
        vector<vector<int>> result;
        result.reserve(1 << size);
        for(size_type it = 0; ; it++) {
            vector<int> current;
            for(size_type i = 0; i < size; i++) {
                if(it & (1 << i)) {
                    current.push_back(nums[i]);
                }
            }
            result.push_back(std::move(current));
            if(it >= max)
                break;
        }
        return result;
    }
};
