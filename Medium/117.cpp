/*
// Definition for a Node.
class Node {
public:
    int val;
    Node* left;
    Node* right;
    Node* next;

    Node() : val(0), left(NULL), right(NULL), next(NULL) {}

    Node(int _val) : val(_val), left(NULL), right(NULL), next(NULL) {}

    Node(int _val, Node* _left, Node* _right, Node* _next)
        : val(_val), left(_left), right(_right), next(_next) {}
};
*/

class Solution {
public:
    Node* connect(Node* root) {
        Node *first = root;
        while(first) {   // depth
            Node *current = first;
            first = nullptr;
            Node **solving = nullptr;
            while(current) {   // width
                Node *left = current->left;
                Node *right = current->right;
                if(left) {
                    if(solving) {
                        *solving = left;
                    } else {
                        first = left;
                    }
                    solving = &(left->next);
                }
                if(right) {
                    if(solving) {
                        *solving = right;
                    } else {
                        first = right;
                    }
                    solving = &(right->next);
                }
                current = current->next;
            }
        }
        return root;
    }
};
