#include <cstring>

class Solution {
public:
    int triangleNumber(vector<int>& nums) {
        // initialize le_count
        int count[1001];
        std::memset(count, 0, sizeof(count));
        for(auto n : nums) {
            count[n]++;
        }
        int le_count[1001];
        int lec = 0;
        for(int i = 0; i <= 1000; i++) {
            lec += count[i];
            le_count[i] = lec;
        }
        // solve
        int ijk_count = 0;
        for(int i = 1; i <= 1000; i++) {
            int _count_i = count[i];
            if(_count_i == 0) { continue; }

            // (i, j, *), j < i
            int jk_count = 0;
            for(int j = (i + 1) / 2; j < i; j++) {
                // (i, j, k), i - j < k < j
                if(j > i - j)
                    jk_count += count[j] * (le_count[j - 1] - le_count[i - j]);
                // (i, j, j)
                if(j + j > i) {
                    jk_count += count[j] * (count[j] - 1) / 2;
                }
            }
            ijk_count += jk_count * _count_i;

            // (i, i, k), k < i
            ijk_count += _count_i * (_count_i - 1) / 2
                * (le_count[i - 1] - le_count[0]);
            // (i, i, i)
            ijk_count += _count_i * (_count_i - 1) * (_count_i - 2) / 6;
        }
        return ijk_count;
    }
};
