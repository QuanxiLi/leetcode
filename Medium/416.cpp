#include <vector>
#include <unordered_set>

using namespace std;

class Solution {
public:
    bool canPartition(vector<int>& nums) {
        unordered_set<int> partial_sums { 0 };
        int sum = 0;
        for(auto v : nums) {
            sum += v;
        }
        if(sum % 2 == 1)
            return false;
        int half_sum = sum / 2;
        int half_sum_minus_left = -half_sum;
        sort(nums.begin(), nums.end(), greater<int>());
        for(auto v : nums) {
            half_sum_minus_left += v;
            unordered_set<int> _ps;
            _ps.reserve(partial_sums.size());
            for(auto &s : partial_sums) {
                int x = s + v;
                if(x < half_sum && x >= half_sum_minus_left) {
                    _ps.insert(s + v);
                } else if(x == half_sum) {
                    return true;
                }
            }
            for(auto &s : partial_sums) {
                if(s >= half_sum_minus_left)
                    _ps.insert(s);
            }
            partial_sums = std::move(_ps);
        }
        return false;
    }
};
