/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode(int x) : val(x), left(NULL), right(NULL) {}
 * };
 */
class Solution {
    TreeNode *buildTree(int *inorder, int *postorder, int size) {
        switch(size) {
        case 0:
            return nullptr;
        case 1:
            return new TreeNode(*inorder);
        case 2:
            {
                TreeNode *root = new TreeNode(postorder[1]);
                TreeNode *leaf = new TreeNode(postorder[0]);
                if(inorder[0] == postorder[0]) {
                    root->left = leaf;
                } else {
                    root->right = leaf;
                }
                return root;
            }
        }
        int root_val = postorder[size - 1];
        TreeNode *root = new TreeNode(root_val);
        for(int i = 0; i < size; i++) {
            if(inorder[i] == root_val) {
                root->left = buildTree(inorder, postorder, i);
                root->right = buildTree(inorder + i + 1, postorder + i, size - i - 1);
                return root;
            }
        }
        // error
        return nullptr;
    }
public:
    TreeNode* buildTree(vector<int>& inorder, vector<int>& postorder) {
        if(inorder.size() != postorder.size())
            return nullptr;
        return buildTree(inorder.data(), postorder.data(), inorder.size());
    }
};
