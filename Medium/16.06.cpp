#include <vector>
#include <algorithm>
#include <climits>

using namespace std;

class Solution {
public:
    int smallestDifference(vector<int>& a, vector<int>& b) {
        int n = a.size();
        int m = b.size();
        if(n > m) {
            a.swap(b);
            n = m;
        }
        sort(a.begin(), a.end());
        long long diff = INT_MAX;
        for(long long v : b) {
            if(a.front() >= v) {
                diff = min(diff, a.front() - v);
                continue;
            }
            if(a.back() <= v) {
                diff = min(diff, v - a.back());
                continue;
            }
            int lb = 0, ub = n - 1;
            while(lb < ub) {
                int mid = (lb + ub) / 2;
                int vm = a[mid];
                if(vm > v) {
                    ub = mid - 1;
                } else if(vm < v) {
                    lb = mid + 1;
                } else {
                    return 0;
                }
            }
            if(ub == lb) {
                if(v < a[ub]) {
                    ub--;
                } else {
                    lb++;
                }
            }
            diff = min(diff, v - a[ub]);
            diff = min(diff, a[lb] - v);
        }
        return diff;
    }
};

