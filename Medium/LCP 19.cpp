#include <string>
#include <vector>

using std::string;

class Solution {
public:
    int minimumOperations(string leaves) {
        // generate block_size
        int count = 0;
        std::vector<int> block_size;
        if(leaves[0] == 'y') {
            count++;
        }
        int current_count = 1;
        char current_color = 'r';
        for(size_t i = 1; i < leaves.size() - 1; i++) {
            char c = leaves[i];
            if(c == current_color) {
                current_count++;
            } else {
                block_size.push_back(current_count);
                current_color = c;
                current_count = 1;
            }
        }
        if(current_color != 'r') {
            block_size.push_back(current_count);
            block_size.push_back(1);
        } else {
            block_size.push_back(current_count + 1);
        }
        if(leaves.back() == 'y') {
            count++;
        }
        // easy case
        if(block_size.size() < 7) {
            if(block_size.size() == 1) {
                return count + 1;
            }
            if(block_size.size() == 5) {
                int best = block_size[1];
                if(block_size[2] < best) {
                    best = block_size[2];
                }
                if(block_size[3] < best) {
                    best = block_size[3];
                }
                return best + count;
            } else {
                return count;
            }
        }
        // generate odd_sum and even_sum
        std::vector<int> odd_sum{0}, even_sum{0};
        int os = 0, es = 0;
        for(int i = 0; i < block_size.size() - 1; i += 2) {
            es += block_size[i];
            os += block_size[i + 1];
            odd_sum.push_back(os);
            even_sum.push_back(es);
        }
        // odd_sum.append(os + block_size.back());
        // search the answer
        int best = leaves.size();
        /*
        // This is too slow
        for(int a = 1; a < block_size.size() - 2; a++) {
            for(int b = a + 1; b < block_size.size() - 1; b++) {
                int op_sum =
                    odd_sum[a / 2] - odd_sum[(b + 1) / 2]
                    + even_sum[(b + 1) / 2] - even_sum[(a + 2) / 2];
                if(op_sum < best) {
                    best = op_sum;
                }
            }
        }
        */

        // list_a[a / 2] := odd_sum[a / 2] - even_sum[(a + 2) / 2]
        // aka. 
        // list_a[i] := odd_sum[i] - even_sum[i + 1]
        // 
        // list_b[(b + 1) / 2] := even_sum[(b + 1) / 2] - odd_sum[(b + 1) / 2]
        // aka. 
        // list_b[i] := even_sum[i] - odd_sum[i]
        std::vector<int> list_a; 
        std::vector<int> list_b;
        
        for(int i = 0; i < odd_sum.size() - 1; i++) {
            list_a.push_back(odd_sum[i] - even_sum[i + 1]);
            list_b.push_back(even_sum[i] - odd_sum[i]);
        }
        {
            int i = odd_sum.size() - 1;
            list_b.push_back(even_sum[i] - odd_sum[i]);
        }
        // best = min(list_a[i] + list_b[j]) where
        //      0 <= i < block_size.size() / 2 and
        //      i + 1 <= j < block_size.size() / 2 + 1
        //      note that: block_size.size() / 2 + 1 == list_b.size()

        // list_min[i] = min(list_b[j]) where
        //      i + 1 <= j < list_b.size()
        // so we have:
        // best = min(list_a[i] + list_min[i])
        std::vector<int> list_min(list_a.size());
        int lb_min = list_b.back();
        for(int j = list_b.size() - 2; j >= 0; j--) {
            list_min[j] = lb_min;
            int lbj = list_b[j];
            if(lbj < lb_min) {
                lb_min = lbj;
            }
        }
                         
        // find the best i
        for(int i = 0; i < list_a.size(); i++) {
            int v = list_a[i] + list_min[i];
            if(v < best) {
                best = v;
            }
        }

        return best + count + odd_sum.back();
    }
};
