#include <cstdint>

class Solution {
public:
    static inline int multiply(int x, int y) {
        std::int64_t result =
            static_cast<std::int64_t>(x) * static_cast<std::int64_t>(y);
        result %= 1000000007;
        return static_cast<int>(result);
    }

    static int calc3exp(int m) {
        const int simple_result[] = {
            1, 3, 9, 27, 81, 243, 729, 2187, 6561, 19683, 59049, 
            177147, 531441, 1594323, 4782969
        };
        // 3^14 = 4782969 < 1e7
        constexpr int sr14 = 4782969;
        int result = simple_result[m % 14];
        int count = m / 14;
        for(int i = 0; i < count; i++) {
            result = multiply(result, sr14);
        }
        return result;
    }

    int cuttingRope(int n) {
        if(n == 2) {
            return 1;
        } else if(n == 3) {
            return 2;
        }
        /*
            理论上：
            f(m) = (n / m) ^ m
            f(m) = exp(m * [ln(n) - ln(m)])
            f'(m) = f(m) * [ln(n) - ln(m) - 1]
            when ln(n) = ln(m) + 1
                m = n / e 
            但实际只要整数  
        */
        /*
            因为4 = 2 * 2，所以拆分中只会有2和3
            2 + 2 + 2 = 3 + 3
            2 * 2 * 2 < 3 + 3
            所以要尽量拆成3
        */
        switch(n % 3) {
            case 0:
                // 3^m
                return calc3exp(n / 3);
            case 1:
                // 3^(m - 1) * 4
                return multiply(calc3exp((n - 4) / 3), 4);
            case 2:
                // 3^(m - 1) * 2
                return multiply(calc3exp((n - 2) / 3), 2);
            default:
                return 0;
        }
    }
};
