#include <algorithm>
#include <vector>

using namespace std;

class Solution {
public:
    vector<int> largestDivisibleSubset(vector<int>& nums) {
        int n = nums.size();
        sort(nums.begin(), nums.end());
        int *childnum = new int[n];

        int max_cn = 0;
        for(int i = 0; i < n; i++) {
            int numsi = nums[i];
            int mcn = 0;
            for(int j = 0; j < i; j++) {
                if(numsi % nums[j] == 0) {
                    mcn = max(mcn, childnum[j]);
                }
            }
            childnum[i] = mcn + 1;
            max_cn = max(max_cn, childnum[i]);
        }

        vector<int> result(max_cn);
        int v, i;
        for(i = n - 1; i >= 0; i--) {
            if(childnum[i] == max_cn) {
                v = nums[i];
                max_cn--;
                result[max_cn] = v;
                break;
            }
        }
        while(max_cn > 0) {
            for(; ; i--) {
                if((childnum[i] == max_cn) && (v % nums[i] == 0)) {
                    break;
                }
            }
            v = nums[i];
            max_cn--;
            result[max_cn] = v;
        }

        delete[] childnum;
        return result;
    }
};
