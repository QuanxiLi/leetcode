/**
 * Definition for singly-linked list.
 * struct ListNode {
 *     int val;
 *     ListNode *next;
 *     ListNode(int x) : val(x), next(NULL) {}
 * };
 */
class Solution {
public:
    ListNode* addTwoNumbers(ListNode* l1, ListNode* l2) {
        int c = 0;
        ListNode *a = l1, *b = l2;
        if(!a) return b;
        while(true) {
            int av = a->val + (b ? b->val + c : c);
            if(av >= 10) {
                av -= 10;
                c = 1;
            } else {
                c = 0;
            }
            a->val = av;
            if(a->next) {
                a = a->next;
                if(b)
                    b = b->next;
            } else if(b && b->next) {
                a->next = b->next;
                a = b->next;
                b = nullptr;
            } else {
                if(c) {
                    a->next = new ListNode(c);
                }
                return l1;
            }
        }
        return nullptr;
    }
};
