class Solution:
    def permuteUnique(self, nums: List[int]) -> List[List[int]]:
        m = {}
        for i in nums:
            if i in m:
                m[i] += 1
            else:
                m[i] = 1
        result = []

        def ____(prefix, left, result, last):
            for i in left.keys():
                if left[i] > 0:
                    if(last == 1):
                        result.append(prefix + [i])
                        return
                    left[i] -= 1
                    ____(prefix + [i], left, result, last - 1)
                    left[i] += 1
                    
        ____([], m, result, len(nums))
        return result


