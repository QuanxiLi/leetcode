class Solution {
public:
    int rob(vector<int>& nums) {
        int n = nums.size();
        switch(n) {
        case 0:
            return 0;
        case 1:
            return nums[0];
        case 2:
            return max(nums[0], nums[1]);
        case 3:
            return max(max(nums[0], nums[1]), nums[2]);
        default:
            break;
        }
        int a1 = 0, b1 = nums[1];
        int a0 = nums[0], b0 = max(a0, b1);
        for(int i = 2; i < n - 1; i++) {
            int k = nums[i];
            int c0 = max(a0 + k, b0);
            int c1 = max(a1 + k, b1);
            a0 = b0; b0 = c0;
            a1 = b1; b1 = c1;
        }
        int c1_last = max(a1 + nums.back(), b1);
        return max(b0, c1_last);
    }
};

