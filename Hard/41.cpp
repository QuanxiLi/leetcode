class Solution {
public:
    int firstMissingPositive(vector<int>& nums) {
        int size = nums.size();
        for(int &n: nums) {
            if(n <= 0) {
                n = size + 1;
            }
        }
        for(int n : nums) {
            int m = n > 0 ? n : -n;
            if(m > size) { continue; }
            int &x = nums[m - 1];
            if(x > 0)
                x = -x;
        }
        for(int m = 0; m < size; m++) {
            if(nums[m] >= 0) {
                return m + 1;
            }
        }
        return size + 1;
    }
};
