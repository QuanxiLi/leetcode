#include <sstream>
#include <string>

using namespace std;

class Solution {
    static inline bool gen1(ostream &os, int num) {
        static const char *single[] = {
            "Zero", "One", "Two", "Three", "Four", "Five", "Six", "Seven", "Eight", "Nine"};
        if (num != 0) {
            if (os.tellp() != 0)
                os << ' ';
            os << single[num];
            return true;
        } else {
            return false;
        }
    }

    static inline bool gen2(ostream &os, int num) {
        static const char *_1x[] = {
            "Ten", "Eleven", "Twelve", "Thirteen", "Fourteen", "Fifteen", "Sixteen",
            "Seventeen", "Eighteen", "Nineteen"};
        static const char *x0[] = {
            "Zero", "Ten", "Twenty", "Thirty", "Forty", "Fifty", "Sixty", "Seventy",
            "Eighty", "Ninety"};
        if (num < 10) {
            return gen1(os, num);
        } else if (num < 20) {
            if (os.tellp() != 0)
                os << ' ';
            os << _1x[num - 10];
        } else {
            if (os.tellp() != 0)
                os << ' ';
            os << x0[num / 10];
            gen1(os, num % 10);
        }
        return true;
    }

    static inline bool generate(ostream &os, int num) {
        int h = num / 100;
        num %= 100;
        if (h > 0) {
            gen1(os, h);
            os << " Hundred";
        }
        return gen2(os, num) || (h > 0);
    }

    static inline void generateWithSuffix(ostream &os, int num, const char *suffix) {
        if (generate(os, num)) {
            os << ' ' << suffix;
        }
    }

public:
    string numberToWords(int num) {
        if (num == 0) {
            return "Zero";
        }
        constexpr int THOUSAND = 1000;
        constexpr int MILLION = THOUSAND * THOUSAND;
        constexpr int BILLION = THOUSAND * MILLION;
        ostringstream ss;
        generateWithSuffix(ss, num / BILLION, "Billion");
        num %= BILLION;
        generateWithSuffix(ss, num / MILLION, "Million");
        num %= MILLION;
        generateWithSuffix(ss, num / THOUSAND, "Thousand");
        num %= THOUSAND;
        generate(ss, num);
        return ss.str();
    }
};
