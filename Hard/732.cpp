#include <map>

using namespace std;

class MyCalendarThree {
    map<unsigned /* start time */, unsigned /* task num */> tasks;
    int k = 0;

public:
    MyCalendarThree() {
        tasks[0] = 0;
    }

    int book(int start, int end) {
        auto it = tasks.lower_bound(start);
        if (it == tasks.end()) {
            // at the end
            tasks.emplace_hint(it, start, 1);
            tasks.emplace_hint(tasks.end(), end, 0);
            if (k < 1)
                k = 1;
            return k;
        }
        int task_num;
        // insert `start`
        if (it->first != start) {
            auto before_start = it;
            before_start--; // ofcourse it != tasks.begin() since tasks.front() == (0, *)
            task_num = before_start->second + 1;
            it = tasks.emplace_hint(it, start, task_num);
        } else {
            task_num = ++it->second;
        }
        if (k < task_num) {
            k = task_num;
        }
        // find `end`
        while (true) {
            it++;
            if (it == tasks.end()) {
                tasks.emplace_hint(it, end, 0);
                break;
            }
            if (it->first == end) {
                break;
            }
            if (it->first > end) {
                // the last one < end
                tasks.emplace_hint(it, end, task_num - 1);
                break;
            }
            // not meet `end` yet
            task_num = ++it->second;
            if (k < task_num) {
                k = task_num;
            }
        }
        return k;
    }
};

/**
 * Your MyCalendarThree object will be instantiated and called as such:
 * MyCalendarThree* obj = new MyCalendarThree();
 * int param_1 = obj->book(start,end);
 */
