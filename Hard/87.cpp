#include <string>
#include <vector>
#include <utility>
#include <unordered_map>
#include <cstring>
#include <algorithm>

using namespace std;

class Solution {
    struct Key {
        int start1, start2, len;
        Key(int _1, int _2, int _3) : start1(_1), start2(_2), len(_3) {}
        bool operator==(const Key &other) const {
            return start1 == other.start1 && start2 == other.start2
                && len == other.len;
        }
    };

    struct HashKey {
        size_t operator()(const Key &key) const {
            return (key.start1 << 16) ^ (key.start2 << 8) ^ (key.len);
        }
    };

    const char *str1, *str2;
    unordered_map<Key, bool, HashKey> cache;
    char *temp1, *temp2;

    bool solve(int start1, int start2, int len) {
        const char *p1 = str1 + start1;
        const char *p2 = str2 + start2;
        switch(len) {
        case 0:
            return true;
        case 1:
            return *p1 == *p2;
        case 2:
            return (p1[0] == p2[0] && p1[1] == p2[1]) 
                || (p1[0] == p2[1] && p1[1] == p2[0]);
        }
        Key key(start1, start2, len);
        auto it = cache.find(key);
        if(it != cache.end()) {
            return it->second;
        }

        memcpy(temp1, p1, len);
        memcpy(temp2, p2, len);
        temp1[len] = 0;
        temp2[len] = 0;
        sort(temp1, temp1 + len);
        sort(temp2, temp2 + len);
        if(strcmp(temp1, temp2) != 0) {
            cache[key] = false;
            return false;
        }

        for(int i = 1; i < len; i++) {
            int j = len - i;
            if((solve(start1, start2, i) && solve(start1 + i, start2 + i, j))
                || (solve(start1 + i, start2, j) && solve(start1, start2 + j, i))
                ) {
                cache[key] = true;
                return true;
            }
        }
        cache[key] = false;
        return false;
    }
public:
    bool isScramble(string s1, string s2) {
        int size = s1.size();
        if(size != s2.size())
            return false;
        str1 = s1.c_str();
        str2 = s2.c_str();
        cache.clear();
        temp1 = new char[size + 1];
        temp2 = new char[size + 1];
        return solve(0, 0, size);
        delete[] temp1;
        delete[] temp2;
    }
};
