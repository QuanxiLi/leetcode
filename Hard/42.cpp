#include <algorithm>
#include <vector>

using std::vector;

class Solution {
public:
    int trap(vector<int>& height) {
        vector<int> left_max;
        int size = height.size();
        left_max.reserve(size);
        int max = 0;
        for(int i = 0; i < size; i++) {
            left_max.push_back(max);
            if(height[i] > max)
                max = height[i];
        }
        max = 0;
        int sum = 0;
        for(int i = size - 1; i >= 0; i--) {
            int water = std::min(max, left_max[i]) - height[i];
            if(water > 0)
                sum += water;
            if(height[i] > max)
                max = height[i];
        }
        return sum;
    }
};
