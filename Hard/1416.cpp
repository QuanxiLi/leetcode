#include <deque>
#include <string>
#include <utility>

constexpr int N = 1000000007;

using namespace std;

class Solution {
public:
    int numberOfArrays(string s, int k) {
        int k_div_10_plus_1 = k / 10 + 1;
        deque<pair<int, int>> m;
        for (auto it = s.begin(); it != s.end(); it++) {
            char c = *it;
            deque<pair<int, int>> next;
            pair<int, int> *val_ptr = nullptr;
            int val = c - '0';
            int last_count = ((it == s.begin()) ? 1 : 0);
            for (auto &x : m) {
                last_count += x.second;
                if (last_count >= N)
                    last_count -= N;
                if (x.first > k_div_10_plus_1)
                    continue;
                int v = x.first * 10 + val;
                if (v <= k) {
                    next.emplace_back(v, x.second);
                }
                if (v == val) {
                    val_ptr = &next.back();
                }
            }
            if (val != 0) {
                if (val_ptr) {
                    val_ptr->second += last_count;
                    if (val_ptr->second >= N)
                        val_ptr->second -= N;
                } else {
                    next.emplace_back(val, last_count);
                }
            }
            m = std::move(next);
        }
        int count = 0;
        for (auto &x : m) {
            count += x.second;
            if (count >= N)
                count -= N;
        }
        return count;
    }
};
