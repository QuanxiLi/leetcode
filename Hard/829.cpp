#include <vector>

using namespace std;

class Solution {
public:
    static inline void __try_add_prime(int x, vector<int> &primes) {
        for (int j : primes) {
            if (j * j > x) {
                primes.push_back(x);
                return;
            }
            if (x % primes[j] == 0)
                return;
        }
    }

    static inline int getNthPrime(int n) {
        static vector<int> primes{2, 3, 5, 7, 11, 13, 17, 19, 23};
        static int searched = 24 / 6;
        while (primes.size() <= n) {
            __try_add_prime(6 * searched + 1, primes);
            __try_add_prime(6 * searched + 5, primes);
            searched++;
        }
        return primes[n];
    }

    int consecutiveNumbersSum(int N) {
        int count = 1;
        /*
            (a + b)(b - a + 1) / 2 = N
            设  m = a + b, n = b - a + 1
            则  m * n = 2N, m > n
            a = (m - n + 1) / 2
            b = (m + n - 1) / 2
            即寻找  m * n = 2N, m > n, (m - n) % 2 = 1的(m, n)对数
            本质上是分解因数问题
            求出2N的所有质因数2N = p1^n1 * p2^n2 * pk^nk
            显然p1 = 2，而m, n不能同时有因子2。
            这样的m, n共2 * (n2 + 1) * ... * (nk + 1)种，
            其中一半满足m > n。
            所以(n2 + 1) * ... * (nk + 1)即为所求
        */
        while (N % 2 == 0)
            N /= 2;
        int i = 1;
        while (N > 1) {
            int p;
            while (true) {
                p = getNthPrime(i);
                if (p * p > N) {
                    p = N;
                    break;
                }
                i++;
                if (N % p == 0)
                    break;
            }
            int pn = 0;
            while (N % p == 0) {
                N /= p;
                pn++;
            }
            count *= pn + 1;
        }
        return count;
    }
};
