#include <algorithm>
#include <cstdint>
#include <queue>
#include <utility>
#include <vector>

using namespace std;

class Solution {
public:
    double mincostToHireWorkers(vector<int> &quality, vector<int> &wage, int K) {
        vector<pair<double, int>> cost_performance;
        int n = quality.size();
        cost_performance.reserve(n);
        for (int i = 0; i < n; i++) {
            cost_performance.emplace_back(static_cast<double>(wage[i]) / quality[i], i);
        }
        sort(cost_performance.begin(), cost_performance.end(),
             [](pair<double, int> &p1, pair<double, int> &p2) {
                 return p1.first < p2.first;
             });
        priority_queue<int> hired_quality; // sorted by quality
        int sum_quality = 0;
        for (int i = 0; i < K; i++) {
            int worker_id = cost_performance[i].second;
            int q = quality[worker_id];
            hired_quality.push(q);
            sum_quality += q;
        }
        double min_cp = cost_performance[K - 1].first;
        double min_sum_wage = sum_quality * min_cp;
        for (int j = K; j < n; j++) {
            int qh = hired_quality.top();
            int work_id = cost_performance[j].second;
            int qj = quality[work_id];
            if (qj >= qh)
                continue;
            hired_quality.pop();
            hired_quality.push(qj);
            sum_quality -= qh - qj;
            double sum_wage = sum_quality * cost_performance[j].first;
            if (sum_wage < min_sum_wage) {
                min_sum_wage = sum_wage;
            }
        }
        return min_sum_wage;
    }
};
