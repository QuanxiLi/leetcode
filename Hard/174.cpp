#include <algorithm>
#include <vector>

using namespace std;

class Solution {
public:
    int calculateMinimumHP(vector<vector<int>>& dungeon) {
        int width = dungeon.front().size();
        int height = dungeon.size();
        int last = 0;
        auto &last_line = dungeon.back();
        for(auto i = last_line.rbegin(); i != last_line.rend(); i++) {
            *i += std::min(0, last);
            last = *i;
        }
        for(int y = height - 2; y >= 0; y--) {
            dungeon[y].back() += std::min(0, dungeon[y + 1].back());
            for(int x = width - 2; x >= 0; x--) {
                dungeon[y][x] += std::min(0, 
                    std::max(dungeon[y + 1][x], dungeon[y][x + 1]));
            }
        }
        int addition = dungeon.front().front();
        if(addition > 0) {
            return 1;
        } else {
            return 1 - addition;
        }
    }
};
