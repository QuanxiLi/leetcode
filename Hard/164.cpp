#include <algorithm>
#include <vector>

using namespace std;

class Solution {
public:
    int maximumGap(vector<int>& nums) {
        int size = nums.size();
        if(size < 2) {
            return 0;
        }
        sort(nums.begin(), nums.end());
        int best = 0;
        for(int i = 1; i < size; i++) {
            int dist = nums[i] - nums[i - 1];
            if(dist > best)
                best = dist;
        }
        return best;
    }
};
