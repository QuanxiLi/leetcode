#include <utility>
#include <algorithm>

/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode() : val(0), left(nullptr), right(nullptr) {}
 *     TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
 *     TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
 * };
 */
class Solution {
    pair<int, int> help(TreeNode *root) {
        int val = root->val;
        if(root->left == nullptr && root->right == nullptr) {
            return make_pair(val, val);
        }
        int max_child_second;
        int max_child_first;
        int max_with_root;
        if(root->left == nullptr || root->right == nullptr) {
            auto child = root->left ? root->left : root->right;
            auto p = help(child);
            max_child_first = p.first;
            max_child_second = p.second;
            max_with_root = max_child_second > 0 ? val + max_child_second : val;
        } else {
            auto pleft = help(root->left);
            auto pright = help(root->right);
            max_child_first = max(pleft.first, pright.first);
            max_child_second = max(pleft.second, pright.second);
            max_with_root = max_child_second > 0 ? val + max_child_second : val;
            max_with_root = max(max_with_root, pleft.second + pright.second + val);
        }
        max_child_second = max(0, max_child_second);
        int first = max(max_with_root, max_child_first);
        return make_pair(first, max_child_second + val);
    }
public:
    int maxPathSum(TreeNode* root) {
        return help(root).first;
    }
};
