class Solution {
    inline void skipBlanks(const char *&p) {
        while (*p == ' ') {
            p++;
        }
    }

    inline bool isDigit(char c) {
        return c >= '0' && c <= '9';
    }

    inline void skipDigits(const char *&p) {
        while (isDigit(*p)) {
            p++;
        }
    }

public:
    bool isNumber(string s) {
        /* (+|-)? digit* .? digit* (e(+|-)?digit+) */
        /* exception: '.' */
        const char *p = s.c_str();
        bool meet_digits = false;
        skipBlanks(p);
        if (*p == '+' || *p == '-') {
            p++;
        }
        if (isDigit(*p)) {
            meet_digits = true;
        }
        skipDigits(p);
        if (*p == '.') {
            p++;
        }
        if (!meet_digits && !isDigit(*p)) {
            return false;
        }
        skipDigits(p);
        if (*p == 'e') {
            p++;
            if (*p == '+' || *p == '-') {
                p++;
            }
            if (!isDigit(*p)) {
                return false;
            }
            p++;
            skipDigits(p);
        }
        skipBlanks(p);
        return *p == '\0';
    }
};
