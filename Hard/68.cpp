#include <vector>
#include <string>
#include <cstring>

using namespace std;

class Solution {
public:
    inline static void generateLastLine(vector<string>& words,
                int start, int end, char *str, int maxWidth) {
        char *p = str;
        for(int i = start; ; i++) {
            strcpy(p, words[i].c_str());
            p += words[i].size();
            if(i == end - 1)
                break;
            *p = ' ';
            p++;
        }
        for(; p < str + maxWidth; p++) {
            *p = ' ';
        }
    }

    inline static void generateLine(vector<string>& words,
                int start, int end, char *str, int ex_blanks, int maxWidth) {
        int blank_count = end - start - 1;
        if(blank_count == 0) {
            generateLastLine(words, start, end, str, maxWidth);
            return;
        }
        int blank_size = ex_blanks / blank_count + 1;
        int more = ex_blanks % blank_count;
        char *p = str;
        for(int i = start; ; i++) {
            strcpy(p, words[i].c_str());
            p += words[i].size();
            if(i == end - 1)
                break;
            for(int j = 0; j < blank_size; j++) {
                *p = ' ';
                p++;
            }
            if(more > 0) {
                more--;
                *p = ' ';
                p++;
            }
        }
    }

    vector<string> fullJustify(vector<string>& words, int maxWidth) {
        int len = -1;
        int start = 0;
        char *buffer = new char[maxWidth + 1];
        buffer[maxWidth] = '\0';
        vector<string> result;
        for(int i = 0; i < words.size(); i++) {
            int last_len = len;
            len += words[i].size() + 1;
            if(len > maxWidth) {
                generateLine(words, start, i, buffer, maxWidth - last_len, maxWidth);
                result.emplace_back(buffer);
                len = words[i].size();
                start = i;
            }
        }
        generateLastLine(words, start, words.size(), buffer, maxWidth);
        result.emplace_back(buffer);
        delete[] buffer;
        return result;
    }
};
