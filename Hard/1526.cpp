class Solution {
public:
    int minNumberOperations(vector<int>& target) {
        int last = 0;
        int oc = 0;
        for(auto v : target) {
            if(v > last) {
                oc += v - last;
            }
            last = v;
        }
        return oc;
    }
};
