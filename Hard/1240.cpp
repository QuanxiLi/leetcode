#include <algorithm>

using namespace std;

class Solution {
	int *buffer;
	int line_count;

	int help(int *state, int max_count) {
		int last_steps = 0;
		int maxw = 0, maxw_i = -1;
		for (int i = 0; i < line_count; i++) {
			if (state[i] > maxw) {
				maxw = state[i];
				maxw_i = i;
			}
		}
		if (max_count <= 0 && maxw > 0)
			return 0;
		if (maxw == 0)
			return max_count;
		int maxh = 1;
		for (int i = maxw_i + 1; i < line_count; i++) {
			if (state[i] == maxw)
				maxh++;
			else
				break;
		}
		for (int i = min(maxw, maxh); i > 0; i--) {
			int next_max_count = max_count - last_steps - 1;
			int *new_state = buffer + next_max_count * line_count;
			if (next_max_count > 0) {
				for (int j = 0; j < maxw_i; j++)
					new_state[j] = state[j];
				for (int j = maxw_i; j < maxw_i + i; j++)
					new_state[j] = state[j] - i;
				for (int j = maxw_i + i; j < line_count; j++)
					new_state[j] = state[j];
			}
			last_steps = max(last_steps, help(new_state, next_max_count) + last_steps);
		}
		return last_steps;
	}

public:

	int tilingRectangle(int n, int m) {
		if (n > m) {
			swap(m, n);
		}
		line_count = n;
		int max_count = m;
		buffer = new int[n * (max_count + 1)];
		int *state = buffer + max_count * line_count;
		for (int i = 0; i < n; i++)
			state[i] = m;
		int last_steps = help(state, max_count);
		delete[] buffer;
		return max_count - last_steps;
	}
};

