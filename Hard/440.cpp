class Solution {
public:
    static int find(int prefix, int n, int &k) {
        int count = 0;
        int times = 1;
        while (true) {
            times *= 10;
            int n_div_times = n / times;
            if (prefix > n_div_times)
                break;
            if (prefix + 1 > n_div_times) {
                count += n - prefix * times + 1;
                break;
            }
            count += times;
        }
        if (k > count) {
            k -= count;
            return 0;
        }

        prefix *= 10;
        for (int i = 0; i < 10; i++) {
            int current = prefix + i;
            if (current > n) {
                return 0;
            }
            k--;
            if (k == 0) {
                return current;
            } else {
                int found = find(current, n, k);
                if (found) {
                    return found;
                }
            }
        }
        return 0;
    }

    int findKthNumber(int n, int k) {
        for (int i = 1; i < 10; i++) {
            k--;
            if (k == 0) {
                return i;
            } else {
                int found = find(i, n, k);
                if (found) {
                    return found;
                }
            }
        }
        return 0;
    }
};
