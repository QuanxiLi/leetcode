#include <vector>
#include <algorithm>

using namespace std;

class Solution {
public:
    double findMedianSortedArrays(vector<int>& nums1, vector<int>& nums2) {
        int n1 = nums1.size();
        int n2 = nums2.size();
        int *arr1 = nums1.data();
        int *arr2 = nums2.data();
        int k = (n1 + n2 + 1) / 2;
        while(true) {
            if(n1 <= n2) {
                if(n1 >= k) {
                    n1 = k;
                    n2 = k;
                } else {
                    // n2 > n1 + n2 - k
                    arr2 += k - n1;
                    k = n1;
                    n2 = n1;
                }
            } else {
                if(n2 >= k) {
                    n1 = k;
                    n2 = k;
                } else {
                    arr1 += k - n2;
                    k = n2;
                    n1 = n2;
                }
            }
            // now n1 = n2 = k
			if (k <= 0)
				break;
			int i = (k - 1) / 2;
			int j = k / 2;
			int x1 = arr1[i];
			int x2 = arr2[i];
			if (x1 < x2) {
				arr1 += j;
				n1 -= j;
				k -= j;
				n2 = j;
			}
			else if (x1 == x2) {
				arr1 += i + 1;
				arr2 += j;
				break;
			}
			else {
				arr2 += j;
				n2 -= j;
				k -= j;
				n1 = j;
			}
		}
        double a;
        if(arr1 == nums1.data()) {
            a = arr2[-1];
        } else if(arr2 == nums2.data()) {
            a = arr1[-1];
        } else {
            a = max(arr1[-1], arr2[-1]);
        }
        if((nums1.size() + nums2.size()) % 2 == 0) {
            double b;
            if(arr1 == nums1.data() + nums1.size()) {
                b = *arr2;
            } else if(arr2 == nums2.data() + nums2.size()) {
                b = *arr1;
            } else {
                b = min(*arr1, *arr2);
            }
            return (a + b) / 2;
        } else {
            return a;
        }
    }
};
