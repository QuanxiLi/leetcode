#include <cstring>
#include <set>
#include <vector>

using namespace std;

class Solution {
public:
    double frogPosition(int n, vector<vector<int>> &edges, int t, int target) {
        if (n == 1) {
            return 1;
        }
        if (target == 1) {
            if (t == 0) {
                return 1;
            } else {
                return 0;
            }
        }
        set<int> *next_to = new set<int>[n + 1];
        int *pre = new int[n + 1];
        memset(pre, 0, sizeof(int) * (n + 1));
        for (auto &e : edges) {
            int from = e[0];
            int to = e[1];
            next_to[from].insert(to);
            next_to[to].insert(from);
        }
        int distance = 0;
        pre[1] = 1;
        set<int> current{1};
        while (true) {
            distance++;
            set<int> nexts;
            for (int i : current) {
                for (int j : next_to[i]) {
                    if (pre[j] == 0)
                        pre[j] = i;
                    if (j == target)
                        goto FOUND;
                    nexts.insert(j);
                }
            }
            current = std::move(nexts);
        }
    FOUND:
        double p = 1;
        int time = 0;
        int k = target;
        while (k != 1) {
            k = pre[k];
            if (k == 1) {
                p /= next_to[k].size();
            } else {
                p /= next_to[k].size() - 1;
            }
            time++;
            if (time > t) {
                p = 0;
                break;
            }
        }
        if (time < t && next_to[target].size() != 1)
            p = 0;
        delete[] next_to;
        delete[] pre;
        return p;
    }
};
