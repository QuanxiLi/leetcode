#include <stack>

using namespace std;

class Solution {
    static inline bool readTagName(const char *&s) {
        for (int i = 0; i < 10; i++) {
            char c = *s++;
            if (c == '>') {
                return i > 0;
            }
            if (c < 'A' || c > 'Z')
                return false;
        }
        return false;
    }

    static inline bool checkTagName(const char *tag, const char *&s) {
        while (*tag != '>') {
            if (*tag != *s)
                return false;
            tag++;
            s++;
        }
        s++;
        return true;
    }

    static inline bool readCData(const char *&s) {
        const char *prefix = "[CDATA[";
        for (const char *p = prefix; *p; p++) {
            if (*s++ != *p)
                return false;
        }
        while (char c = *s++) {
            if (c == ']' && *s == ']' && *(s + 1) == '>') {
                s++;
                return true;
            }
        }
        return false;
    }

public:
    bool isValid(string code) {
        const char *str = code.c_str();
        stack<const char *> tags;
        if (*str++ != '<')
            return false;
        tags.push(str);
        if (!readTagName(str))
            return false;
        while (char c = *str++) {
            if (c == '<') {
                c = *str;
                if (c == '!') {
                    if (!readCData(++str))
                        return false;
                } else if (c == '/') {
                    if (tags.empty())
                        return false;
                    if (!checkTagName(tags.top(), ++str))
                        return false;
                    tags.pop();
                    if (tags.empty() && *str)
                        return false;
                } else {
                    tags.push(str);
                    if (!readTagName(str))
                        return false;
                }
            }
        }
        return tags.empty();
    }
};
