#include <random>
#include <unordered_map>
#include <vector>

using namespace std;

class RandomizedCollection {
    struct Node {
        int val;
        int last, next;
        Node(int val, int next)
            : val(val), last(-1), next(next) {}
        Node &operator=(const Node &other) {
            this->val = other.val;
            this->last = other.last;
            this->next = other.next;
            return *this;
        }
    };

    unordered_map<int, int> first_pos;
    vector<Node> data;
    static inline int randint(int n) {
        static std::random_device rd;
        static std::minstd_rand gen(rd());
        std::uniform_int_distribution<> dis(0, n - 1);
        return dis(gen);
    }

public:
    /** Initialize your data structure here. */
    RandomizedCollection() {
        // nothing
    }

    /** Inserts a value to the collection. Returns true if the collection did not already contain the specified element. */
    bool insert(int val) {
        auto it = first_pos.find(val);
        if (it == first_pos.end()) {
            first_pos[val] = data.size();
            data.emplace_back(val, -1);
            return true;
        } else {
            int pos = it->second;
            data[pos].last = data.size();
            it->second = data.size();
            data.emplace_back(val, pos);
            return false;
        }
    }

    /** Removes a value from the collection. Returns true if the collection contained the specified element. */
    bool remove(int val) {
        auto it = first_pos.find(val);
        if (it == first_pos.end()) {
            return false;
        }
        int pos = it->second;
        int next_pos = data[pos].next;
        if (next_pos == -1) {
            first_pos.erase(it);
        } else {
            it->second = next_pos;
            data[next_pos].last = -1;
        }
        if (pos == data.size() - 1) {
            data.pop_back();
        } else {
            auto &bv = data.back();
            data[pos] = bv;
            data.pop_back();
            if (bv.last == -1) {
                first_pos[bv.val] = pos;
            } else {
                data[bv.last].next = pos;
            }
            if (bv.next != -1) {
                data[bv.next].last = pos;
            }
        }
        return true;
    }

    /** Get a random element from the collection. */
    int getRandom() {
        int i = randint(data.size());
        return data[i].val;
    }
};

/**
 * Your RandomizedCollection object will be instantiated and called as such:
 * RandomizedCollection* obj = new RandomizedCollection();
 * bool param_1 = obj->insert(val);
 * bool param_2 = obj->remove(val);
 * int param_3 = obj->getRandom();
 */
