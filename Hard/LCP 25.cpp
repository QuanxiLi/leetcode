#include <algorithm>
#include <cstring>
#include <map>

using namespace std;

constexpr int N = 1000000007;

class Solution {
    int k;

    int **c_cache;
    map<int, int> f_cache[27];

    inline void init(int n) {
        init_c_cache(n);
    }

    inline void destroy(int n) {
        destroy_c_cache(n);
    }

    inline void destroy_c_cache(int n) {
        for (int i = 0; i <= n; i++) {
            delete[] c_cache[i];
        }
        delete[] c_cache;
    }

    inline void init_c_cache(int n) {
        c_cache = new int *[n + 1];
        for (int i = 0; i <= n; i++) {
            c_cache[i] = new int[i + 1];
            memset(c_cache[i], 0, sizeof(int) * (i + 1));
        }
    }

    inline int c(int n, int i) {
        int &cached_val = c_cache[n][i];
        if (cached_val == 0) {
            int val = c_impl(n, i);
            cached_val = val;
            return val;
        } else {
            return cached_val;
        }
    }

    int c_impl(int n, int i) {
        if (i == 0) {
            return 1;
        }
        if (i + i > n) {
            return c(n, n - i);
        }
        return static_cast<int64_t>(n - i + 1) * c(n, i - 1) / i;
    }

    // m键n次输入
    int f(int m, int n) {
        auto &cache = f_cache[m];
        auto it = cache.find(n);
        if (it == cache.end()) {
            int val = f_impl(m, n);
            cache[n] = val;
            return val;
        } else {
            return it->second;
        }
    }

    int f_impl(int m, int n) {
        if (n > k * m) {
            return 0;
        }
        if (m == 1 || n == 0) {
            return 1;
        }
        int64_t count = 0;
        int min_nk = min(n, k);
        for (int i = 0; i <= min_nk; i++) {
            count += static_cast<int64_t>(c(n, i)) * f(m - 1, n - i);
        }
        return count % 1000000007;
    }

public:
    int keyboard(int k, int n) {
        this->k = k;
        init(n);
        int result = f(26, n);
        destroy(n);
        return result;
    }
};
