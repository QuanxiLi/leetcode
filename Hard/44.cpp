#include <string>
#include <set>

using namespace std;


class Solution {
    
public:
    bool isMatch(string s, string p) {
        char *pattern = new char[p.size() + 1];
        char last = 0;
        char *cur = pattern;
        for(auto c : p) {
            if(c == '*' && last == '*') {
                continue;
            }
            last = c;
            *cur = c;
            cur++;
        }
        *cur = 0;
        int pattern_size = cur - pattern;

        set<int> last_state{ 0 };
        if(pattern[0] == '*') {
            last_state.insert(1);
        }
        for(char c : s) {
            set<int> next_state;
            if(last_state.empty()) {
                return false;
            }
            for(auto state : last_state) {
                char pattern_c = pattern[state];
                if(pattern_c == '*') {
                    next_state.insert(state);
                    next_state.insert(state + 1);
                }
                if(pattern_c == c || pattern_c == '?') {
                    next_state.insert(state + 1);
                    if(pattern[state + 1] == '*') {
                        next_state.insert(state + 2);
                    }
                }
            }
            last_state = std::move(next_state);
        }
        int goal = pattern_size;
        if(pattern_size >= 1 && pattern[pattern_size - 1] == '*') {
            goal = pattern_size - 1;
        }
        auto found = last_state.find(goal);
        return found != last_state.end();
    }
};
