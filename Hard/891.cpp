#include <algorithm>
#include <vector>

using namespace std;

constexpr int N = 1000000007;

class Solution {
public:
    int sumSubseqWidths(vector<int> &A) {
        int *x = A.data();
        int n = A.size();
        sort(x, x + n);
        int w_sum = 0;
        int p = 0;
        int q = 0;
        for (int i = n - 2; i >= 0; i--) {
            p = (2 * p + x[i + 1]) % N;
            q = 2 * q + 1;
            if (q >= N) {
                q -= N;
            }
            int64_t _w_sum = w_sum + p - static_cast<int64_t>(x[i]) * q;
            w_sum = _w_sum % N;
            if (w_sum < 0) {
                w_sum += N;
            }
        }
        return w_sum;
    }
};
