#include <map>
#include <stack>
#include <vector>
#include <string>
#include <queue>
#include <set>
using namespace std;


class Solution {
    struct Node {
        map<char, Node*> children;

        ~Node() {
            for(auto &kv : children) {
                delete kv.second;
            }
        }

        void addWord(const char *p) {
            if(*p == '\0') {
                children['\0'] = nullptr;
                return;
            }
            auto it = children.find(*p);
            if(it == children.end()) {
                children[*p] = new Node;
            }
            children[*p]->addWord(p + 1);
        }

        void findWord(const char *p, stack<const char *> &ends) {
            if(children.find('\0') != children.end()) {
                ends.push(p);
            }
            if(*p == '\0' || children.find(*p) == children.end()) {
                return;
            }
            children[*p]->findWord(p + 1, ends);
        }
    };

	struct PrefixNode {
		const char *p;
		set<PrefixNode *> succ;
		vector<string> prefix;
		bool check_val;
		PrefixNode(const char *p) : p(p) {}
	};


public:
    vector<string> wordBreak(string s, vector<string>& wordDict) {
        Node root;
        for(auto &word : wordDict) {
            root.addWord(word.c_str());
        }
        const char *str = s.c_str();
        vector<string> result;
		int size_plus_1 = s.size() + 1;

		PrefixNode **prefix_nodes = new PrefixNode*[size_plus_1];
		for(int i = 0; i < size_plus_1; i++) {
			prefix_nodes[i] = nullptr;
		}
		prefix_nodes[0] = new PrefixNode(str);
		for(int i = 0; i < s.size(); i++) {
			auto current = prefix_nodes[i];
			if(current == nullptr) { continue; }
			// cerr << "current: " << string(str, current->p) << endl;
			stack<const char *> next;
			root.findWord(current->p, next);
			while(!next.empty()) {
				const char *p = next.top();
				// cerr << "\tnext: " << string(current->p, p) << endl;
				if(prefix_nodes[p - str] == nullptr) {
					prefix_nodes[p - str] = new PrefixNode(next.top());
				}
				next.pop();
				current->succ.insert(prefix_nodes[p - str]);
			}
		}

		auto final_node = prefix_nodes[s.size()];
		if(final_node == nullptr) {
			return {};
		}
		final_node->check_val = true;
		for(int i = s.size() - 1; i >= 0; i--) {
			// cerr << "checking " << i << endl;
			auto node = prefix_nodes[i];
			if(node == nullptr) continue;
			// check it
			bool suc = false;
			for(PrefixNode *child : node->succ) {
				if(child->check_val) {
					suc = true;
				} else {
					node->succ.erase(child);
				}
			}
			node->check_val = suc;
			// cerr << "checking `" << node->p << "`: " << suc << endl;
		}

		prefix_nodes[0]->prefix.push_back("");

		for(int i = 0; i < size_plus_1; i++) {
			auto node = prefix_nodes[i];
			if(node == nullptr) continue;
			if(*(node->p) == '\0') {
				result = std::move(node->prefix);
			} else {
				for(auto succ : node->succ) {
					for(auto &pref : node->prefix) {
						if(pref.empty()) {
							succ->prefix.push_back(string(node->p, succ->p));
						} else {
							succ->prefix.push_back(pref + ' ' + string(node->p, succ->p));
						}
					}
				}
			}
			delete node;
		}

		delete[] prefix_nodes;
        return result;
    }
};
