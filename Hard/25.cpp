/**
 * Definition for singly-linked list.
 * struct ListNode {
 *     int val;
 *     ListNode *next;
 *     ListNode(int x) : val(x), next(NULL) {}
 * };
 */
class Solution {
    inline static ListNode *reverse(ListNode *start, ListNode *end) {
        ListNode *next = start->next;
        start->next = end;
        while(next != end) {
            ListNode *temp = next->next;
            next->next = start;
            start = next;
            next = temp;
        }
        return start;
    }
public:
    ListNode* reverseKGroup(ListNode* head, int k) {
        if(k <= 1)
            return head;
        int count = 0;
        ListNode *start = head;
        ListNode *end = start;
        ListNode **last_end = &head;
        while(true) {
            if(end == nullptr)
                break;
            ListNode *last = end;
            end = end->next;
            count++;
            if(count == k) {
                *last_end = reverse(start, end);
                count = 0;
                last_end = &(start->next);
                start = end;
            }
        }
        return head;
    }
};
