#include <string>
#include <climits>
#include <algorithm>

using namespace std;

class Solution {
public:
	int longestAwesome(string s) {
		int n = s.size();
		int first[1024];
		int last[1024];
		for (int i = 0; i < 1024; i++) {
			first[i] = INT_MAX;
			last[i] = 0;
		}
		int current = 0;
		for (int i = 0; i < n; i++) {
			if (first[current] > i)
				first[current] = i;
			last[current] = i;
			current ^= (1 << (s[i] - '0'));
		}
		if (first[current] > n)
			first[current] = n;
		last[current] = n;
		int ml = 0;
		for (int i = 0; i < 1024; i++) {
			int f = first[i];
			if (f == INT_MAX)
				continue;
			int l = last[i];
			l = max(l, last[i ^ (1 << 0)]);
			l = max(l, last[i ^ (1 << 1)]);
			l = max(l, last[i ^ (1 << 2)]);
			l = max(l, last[i ^ (1 << 3)]);
			l = max(l, last[i ^ (1 << 4)]);
			l = max(l, last[i ^ (1 << 5)]);
			l = max(l, last[i ^ (1 << 6)]);
			l = max(l, last[i ^ (1 << 7)]);
			l = max(l, last[i ^ (1 << 8)]);
			l = max(l, last[i ^ (1 << 9)]);
			ml = max(ml, l - f);
		}
		return ml;
	}
};
