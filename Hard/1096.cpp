#include <string>
#include <set>
#include <vector>
#include <cassert>

using namespace std;

class Solution {
	struct Expr {
		set<string> sset;
		virtual ~Expr() {}
		virtual void gensset() = 0;
	};

	struct ExprSet : public Expr {
		vector<Expr *> subexprs;
		virtual void gensset() {
			for (auto e : subexprs) {
				e->gensset();
				for (auto &s : e->sset) {
					sset.insert(move(s));
				}
				delete e;
			}
			if (sset.empty()) {
				sset.insert("");
			}
			subexprs.clear();
		}
	};

	struct ExprList : public Expr {
		vector<Expr *> subexprs;
		virtual void gensset() {
			if (subexprs.empty()) {
				sset.insert("");
				return;
			}
			subexprs[0]->gensset();
			sset = move(subexprs[0]->sset);
			for (auto i = 1; i < subexprs.size(); i++) {
				subexprs[i]->gensset();
				set<string> new_sset;
				for (auto &s2 : subexprs[i]->sset) {
					for (auto &s1 : sset) {
						new_sset.insert(s1 + s2);
					}
				}
				sset = move(new_sset);
				delete subexprs[i];
			}
			subexprs.clear();
		}
	};

	struct ExprChar : public Expr {
		char c;
		virtual void gensset() {
			sset.emplace(1, c);
		}
	};

	static Expr *parseSet(const char *&expression) {
		ExprSet *expr = new ExprSet;
		// assert(expression[0] == '{')
		expression++;
		while (*expression != '}') {
			Expr *subexpr = parse_(expression);
			expr->subexprs.push_back(subexpr);
			if (*expression != ',') {
				assert(*expression == '}');
			}
			else {
				expression++;
			}
		}
		expression++;
		return expr;
	}

	static Expr *parse_(const char *&expression) {
		vector<Expr *> exprlist;
		while (true) {
			char c = expression[0];
			if (c == '{') {
				Expr *expr = parseSet(expression);
				exprlist.push_back(expr);
			}
			else if (c >= 'a' && c <= 'z') {
				ExprChar *expr = new ExprChar;
				expr->c = c;
				expression++;
				exprlist.push_back(expr);
			}
			else {
				break;
			}
		}
		if (exprlist.size() == 1) {
			return exprlist[0];
		}
		else {
			ExprList *expr = new ExprList;
			expr->subexprs = move(exprlist);
			return expr;
		}
	}

	static Expr *parse(const char *expression) {
		Expr *expr = parse_(expression);
		assert(*expression == '\0');
		return expr;
	}
public:
	vector<string> braceExpansionII(string expression) {
		/*
			Expr -> '{' List '}' | Expr Expr | [a-z]
			List -> Expr | Expr ',' List
		*/
		Expr *expr = parse(expression.c_str());
		expr->gensset();
		vector<string> result;
		for (auto &s : expr->sset) {
			result.push_back(move(s));
		}
		return result;
	}
};
