#include <cstdint>
#include <utility>
#include <vector>

constexpr int N = 1000000007; // N is prime

class Fancy {
    std::vector<int> data;
    int64_t a = 1;
    int64_t b = 0;
    // a * data + b
public:
    Fancy() {
    }

    static int64_t getPow(int64_t a, int n) {
        if (n == 0) {
            return 1;
        } else {
            int64_t r = getPow(a * a % N, n / 2);
            if (n % 2 == 1) {
                return r * a % N;
            } else {
                return r;
            }
        }
    }

    static inline int64_t getInverse(int64_t a) {
        // N - 2 = 111011100110101100101000000101B
        return getPow(a, N - 2);
    }

    void append(int val) {
        // val = a * x + b (mod N), get the `x`
        // a^N = a (mod N), so a^(N-2) = a^(-1) (mod N)
        // x = (val - b) * a^(N-2)
        val -= b;
        if (val < 0)
            val += N;
        int x = (val * getInverse(a)) % N;
        data.push_back(x);
    }

    void addAll(int inc) {
        b = (b + inc) % N;
    }

    void multAll(int m) {
        a = (a * m) % N;
        b = (b * m) % N;
    }

    int getIndex(int idx) {
        if (idx < 0 || idx >= data.size()) {
            return -1;
        }
        return (a * data[idx] + b) % N;
    }
};

/**
 * Your Fancy object will be instantiated and called as such:
 * Fancy* obj = new Fancy();
 * obj->append(val);
 * obj->addAll(inc);
 * obj->multAll(m);
 * int param_4 = obj->getIndex(idx);
 */
