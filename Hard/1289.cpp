#include <vector>
#include <algorithm>
#include <climits>

using namespace std;

class Solution {
public:
    int minFallingPathSum(vector<vector<int>>& arr) {
        int m = arr.size();
        int n = arr[0].size();
        int min_v = 0;
        int min_i = -1;
        int min2_v = 0;
        for(auto &line : arr) {
            int new_min_v = INT_MAX;
            int new_min_i = -1;
            int new_min2_v = INT_MAX;
            for(int i = 0; i < n; i++) {
                int v = line[i];
                if(i == min_i) {
                    v += min2_v;
                } else {
                    v += min_v;
                }
                if(v < new_min_v) {
                    new_min2_v = new_min_v;
                    new_min_v = v;
                    new_min_i = i;
                } else if(v < new_min2_v) {
                    new_min2_v = v;
                }
            }
            min_v = new_min_v;
            min2_v = new_min2_v;
            min_i = new_min_i;
        }
        return min_v;
    }
};
