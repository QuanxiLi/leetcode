class Solution {
public:
    vector<vector<int>> insert(vector<vector<int>>& intervals, vector<int>& newInterval) {
        vector<vector<int>> result;
        int start = newInterval[0];
        int end = newInterval[1];
        auto it = intervals.begin();
        while(it != intervals.end()) {
            int _end = (*it)[1];
            if(_end < start) {
                result.push_back(std::move(*it));
                it++;
                continue;
            }
            // end >= start
            int _start = (*it)[0];
            if(_start < start) {
                // [ ( ] )
                start = _start;
            }
            while(_start <= end) {
                if(_end > end)
                    end = _end;
                it++;
                if(it == intervals.end()) {
                    break;
                }
                _start = (*it)[0];
                _end = (*it)[1];
            }
            break;
        }
        vector<int> v{ start, end };
        result.push_back(std::move(v));
        while(it != intervals.end()) {
            result.push_back(std::move(*it));
            it++;
        }
        return std::move(result);
    }
};
