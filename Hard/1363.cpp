#include <vector>
#include <string>
#include <algorithm>

using namespace std;

class Solution {
    static bool del(int *count, int a) {
        if(count[a]) { count[a]--; return true; }
        if(count[a + 3]) { count[a + 3]--; return true; }
        if(count[a + 6]) { count[a + 6]--; return true; }
        return false;
    }
public:
    string largestMultipleOfThree(vector<int>& digits) {
        int count[10] = {0};
        for(auto i : digits) {
            count[i]++;
        }
        int x = 0;
        for (int i = 1; i < 9; i++) {
            x += count[i] * i;
        }
        if (count[0] == digits.size() && digits.size() > 0) {
            return "0";
        }
        switch (x % 3) {
        case 1:
            if (!del(count, 1)) {
                del(count, 2);
                del(count, 2);
            }
            break;
        case 2:
            if (!del(count, 2)) {
                del(count, 1);
                del(count, 1);
            }
            break;
        }
        string s;
        s.reserve(digits.size());
        for(int i = 9; i >= 0; i--) {
            char c = '0' + i;
            int counti = count[i];
            for (int j = 0; j < counti; j++) {
                s.push_back(c);
            }
        }
        return s;
    }
};
