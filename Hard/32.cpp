#include <vector>
#include <string>
#include <utility>
#include <algorithm>

using namespace std;

class Solution {
public:
    int longestValidParentheses(string s) {
        bool changed = true;
        int n = s.size();
        vector<pair<int, int>> brakets;
        for(int i = 0; i < n - 1;) {
            if(s[i] == '(' && s[i + 1] == ')') {
                brakets.emplace_back(i, i + 1);
                i += 2;
            } else {
                i++;
            }
        }
        while(changed) {
            changed = false;
            // (*)
            for(auto &p : brakets) {
                while(true) {
                    int i = p.first;
                    int j = p.second;
                    if(i > 0 && j < n - 1 && s[i - 1] == '(' && s[j + 1] == ')') {
                        p.first--;
                        p.second++;
                        changed = true;
                    } else {
                        break;
                    }
                }
            }
            // ()()
            vector<pair<int, int>> new_brakets;
            for(int k = 0; k < brakets.size();) {
                int i = brakets[k].first;
                int j = brakets[k].second;
                while(++k < brakets.size()) {
                    if(brakets[k].first == j + 1) {
                        j = brakets[k].second;
                        changed = true;
                    } else {
                        break;
                    }
                }
                new_brakets.emplace_back(i, j);
            }
            brakets = move(new_brakets);
        }
        int maxlen = 0;
        for(auto p : brakets) {
            maxlen = max(maxlen, p.second - p.first + 1);
        }
        return maxlen;
    }
};
