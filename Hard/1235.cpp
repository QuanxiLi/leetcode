#include <algorithm>
#include <vector>

using namespace std;

class Solution {
public:
    int jobScheduling(vector<int> &startTime, vector<int> &endTime, vector<int> &profit) {
        int n = profit.size();
        if (n == 0)
            return 0;
        int *work_id = new int[n];
        for (int i = 0; i < n; i++) {
            work_id[i] = i;
        }
        sort(work_id, work_id + n, [&](int i, int j) {
            return endTime[i] < endTime[j];
        });

        int *max_profit = new int[n];

        max_profit[0] = profit[work_id[0]];
        for (int i = 1; i < n; i++) {
            int wid = work_id[i];
            int time = startTime[wid];
            int p;
            if (endTime[work_id[0]] > time) {
                p = 0;
            } else {
                int a = 0, b = i - 1;
                while (a < b) {
                    int m = (a + b + 1) / 2;
                    if (endTime[work_id[m]] <= time) {
                        a = m;
                    } else {
                        b = m - 1;
                    }
                }
                p = max_profit[a];
            }
            max_profit[i] = max(max_profit[i - 1], p + profit[wid]);
        }

        int money = max_profit[n - 1];
        delete[] work_id;
        delete[] max_profit;
        return money;
    }
};
