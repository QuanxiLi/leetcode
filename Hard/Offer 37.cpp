#include <string>
#include <stack>

using namespace std;

/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode(int x) : val(x), left(NULL), right(NULL) {}
 * };
 */

#define NULL_CODE   0xFF
#define CHAR_MASK   0xFF
#define ESCAPE_CODE 0xFE

class Codec {
public:

    // Encodes a tree to a single string.
    string serialize(TreeNode* root) {
        string s;
        stack<TreeNode *> stk;
        stk.push(root);
        while(!stk.empty()) {
            TreeNode *node = stk.top();
            stk.pop();
            if(node == nullptr) {
                s.push_back(NULL_CODE);
            } else {
                unsigned val = *reinterpret_cast<unsigned *>(&node->val);
                if((val & ESCAPE_CODE) == ESCAPE_CODE) {
                    s.push_back(ESCAPE_CODE);
                }
                s.push_back(val & CHAR_MASK);
                val >>= 8;
                s.push_back(val & CHAR_MASK);
                val >>= 8;
                s.push_back(val & CHAR_MASK);
                val >>= 8;
                s.push_back(val & CHAR_MASK);
                stk.push(node->right);
                stk.push(node->left);
            }
        }
        return s;
    }

    // Decodes your encoded data to tree.
    TreeNode* deserialize(string data) {
        TreeNode *result = nullptr;
        stack<TreeNode **> stk;
        stk.push(&result);
        int i = 0;
        while(!stk.empty()) {
            TreeNode **current = stk.top();
            stk.pop();
            char c = data[i];
            if(c == (char)NULL_CODE) {
                // nullptr
                i++;
            } else {
                // int
                if(c == (char)ESCAPE_CODE)
                    i++;
                unsigned val = (unsigned char)data[i];
                val |= (unsigned)(unsigned char)data[i + 1] << 8;
                val |= (unsigned)(unsigned char)data[i + 2] << 16;
                val |= (unsigned)(unsigned char)data[i + 3] << 24;
                i += 4;
                auto p = new TreeNode(*reinterpret_cast<int *>(&val));
                *current = p;
                stk.push(&(p->right));
                stk.push(&(p->left));
            }
        }
        return result;
    }
};

// Your Codec object will be instantiated and called as such:
// Codec codec;
// codec.deserialize(codec.serialize(root));
