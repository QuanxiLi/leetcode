#include <algorithm>
#include <vector>

using namespace std;

class Solution {
    static inline bool a_gt_2b(int a, int b) {
        int h = a / 2;
        return (h > b) || (h == b) && (a > 0) && (a & 1);
    }

    static int reversePairsHelp(int a, int b, vector<int> &nums) {
        switch (b - a) {
        case 0:
            return 0;
        case 1: {
            int result = a_gt_2b(nums[a], nums[b]) ? 1 : 0;
            if (nums[a] > nums[b]) {
                swap(nums[a], nums[b]);
            }
            return result;
        }
        default:
            break;
        }
        int m = (a + b) / 2;
        int count = 0;
        count += reversePairsHelp(a, m, nums);
        count += reversePairsHelp(m + 1, b, nums);
        int j = m + 1;
        for (int i = a; i <= m; i++) {
            while (j <= b) {
                if (a_gt_2b(nums[i], nums[j])) {
                    count += m - i + 1;
                    j++;
                } else {
                    break;
                }
            }
        }
        sort(nums.data() + a, nums.data() + b + 1);
        return count;
    }

public:
    int reversePairs(vector<int> &nums) {
        int n = nums.size();
        if (n <= 1)
            return 0;
        return reversePairsHelp(0, n - 1, nums);
    }
};
