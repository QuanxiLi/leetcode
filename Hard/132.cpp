#include <string>
#include <algorithm>
using namespace std;

class Solution {
public:
    int minCut(string s) {
        int n = s.size();
        switch(n) {
        case 0:
        case 1:
            return 0;
        case 2:
            return (s[0] == s[1]) ? 0 : 1;
        case 3:
            return (s[0] == s[2]) ? 0 : 1;
        default:
            break;
        }
        bool *palindrome = new bool[n * (n + 1) / 2];
        bool **palindrome_start = new bool*[n];
        // l = 0
        for(int i = 0; i < n; i++) {
            palindrome[i] = true;
        }
        palindrome_start[0] = palindrome;
        // l = 1
        bool *_sp = palindrome + n;
        palindrome_start[1] = _sp;
        for(int i = 0; i < n - 1; i++) {
            _sp[i] = s[i] == s[i + 1];
        }
        // l = 2
        _sp += n - 1;
        palindrome_start[2] = _sp;
        for(int i = 0; i < n - 2; i++) {
            _sp[i] = s[i] == s[i + 2];
        }
        // l >= 3
        _sp += n - 2;
        for(int l = 3; l < n; l++) {
            palindrome_start[l] = _sp;
            bool *_llsp = palindrome_start[l - 2];
            for(int i = 0; i < n - l; i++) {
                _sp[i] = (s[i] == s[i + l]) && _llsp[i + 1];
            }
            _sp += n - l;
        }
        
        int *subsn = new int[n];
        subsn[0] = 1;
        for(int j = 1; j < n; j++) {
            if(palindrome_start[j][0]) {
                subsn[j] = 1;
                continue;
            }
            int subsn_j = j + 1;
            for(int l = 0; l < j; l++) {
                int i = j - l;
                if(palindrome_start[l][i]) {
                    subsn_j = min(subsn_j, subsn[i - 1] + 1);
                }
            }
            subsn[j] = subsn_j;
        }
        int result = subsn[n - 1] - 1;

        delete[] subsn;
        delete[] palindrome;
        delete[] palindrome_start;

        return result;
    }
};
