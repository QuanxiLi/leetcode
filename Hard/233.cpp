#include <utility>
using namespace std;

class Solution {
    static pair<int, int> help(int n) {
        if(n < 10) {
            return make_pair((n > 1 ? 1 : 0), (n == 1 ? 1 : 0));
        }
        int x = n / 10;
        auto p = help(x);
        int first = p.first * 10 + p.second * (n % 10) + (static_cast<unsigned>(n) + 8) / 10;
        int second = p.second + (n % 10 == 1 ? 1 : 0);
        return make_pair(first, second);
    }
public:
    int countDigitOne(int n) {
        auto p = help(n);
        return p.first + p.second;
    }
};
