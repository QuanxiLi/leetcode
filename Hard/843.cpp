#include <climits>
#include <cstdint>
#include <string>
#include <set>
#include <algorithm>

using namespace std;

#define MINDEX(A, B)    (A) * ((A) - 1) / 2 + (B)
#define MATCH_(A, B)    match[MINDEX(A, B)]
#define MATCH(A, B)     (((A) > (B)) ? MATCH_(A, B) : MATCH_(B, A))

/**
 * // This is the Master's API interface.
 * // You should not implement it, or speculate about its implementation
 * class Master {
 *   public:
 *     int guess(string word);
 * };
 */
class Solution {
public:
    void findSecretWord(vector<string>& wordlist, Master& master) {
        int count = wordlist.size();
        int8_t *match = new int8_t[count * (count - 1) / 2];
        for(int a = 1; a < count; a++) {
            for(int b = 0; b < a; b++) {
                int m = 0;
                auto &as = wordlist[a];
                auto &bs = wordlist[b];
                for(int i = 0; i < 6; i++) {
                    if(as[i] == bs[i])  m++;
                }
                MATCH_(a, b) = m;
            }
        }

        set<int> left;
        for(int i = 0; i < count; i++) {
            left.insert(i);
        }
        for(int t = 0; t < 10; t++) {
            int best_mclist[6] = 
                { INT_MAX, INT_MAX, INT_MAX, INT_MAX, INT_MAX, INT_MAX };
            int best_index = -1;
            for(int i = 0; i < count; i++) {
                // build mclist
                int mclist[7];
                for(int j = 0; j < 6; j++) { mclist[j] = 0; }
                for(int j : left) {
                    if(i == j) continue;
                    mclist[MATCH(i, j)]++;
                }
                sort(mclist, mclist + 6, std::greater<int>());
                // compare mclist with best_mclist
                for(int j = 0; j < 6; j++) {
                    if(best_mclist[j] > mclist[j]) {
                        for(int k = 0; k < 6; k++) {
                            best_mclist[k] = mclist[k];
                            best_index = i;
                            goto NEXT_I;
                        }
                    } else if(best_mclist[j] < mclist[j]) {
                        goto NEXT_I;
                    } else {
                        continue;
                    }
                }
                if(left.find(best_index) == left.end() && left.find(i) != left.end()) {
                    for(int k = 0; k < 6; k++) {
                        best_mclist[k] = mclist[k];
                        best_index = i;
                        goto NEXT_I;
                    }
                }
            NEXT_I:
                continue;
            }
            // now we get best_index
            int guess_result = master.guess(wordlist[best_index]);
            if(guess_result == 6) return;
            set<int> new_left;
            for(int i : left) {
                if(i == best_index) continue;
                if(MATCH(best_index, i) == guess_result) {
                    new_left.insert(i);
                }
            }
            if(new_left.size() == 1) {
                master.guess(wordlist[*(new_left.begin())]);
                return;
            }
            left = std::move(new_left);
        }
    }
};
