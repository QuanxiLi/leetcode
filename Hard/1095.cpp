/**
 * // This is the MountainArray's API interface.
 * // You should not implement it, or speculate about its implementation
 * class MountainArray {
 *   public:
 *     int get(int index);
 *     int length();
 * };
 */

class Solution {
public:
    int findInMountainArray(int target, MountainArray &mountainArr) {
        int n = mountainArr.length();
        int peak_min = 0;
        int peak_max = n - 2;
        int la = 0, lb = n - 1;
        int ra = 0, rb = n - 1;
        // find the peak
        while(peak_min < peak_max) {
            int mid = (peak_min + peak_max) / 2;
            int a = mountainArr.get(mid);
            int b = mountainArr.get(mid + 1);
            if(a > b) {
                peak_max = mid;
                if(a > target) {
                    ra = mid + 1;
                }
                if(b < target) {
                    rb = mid;
                }
            } else {
                peak_min = mid + 1;
                if(a < target) {
                    la = mid + 1;
                }
                if(b > target) {
                    lb = mid;
                }
            }
        }
        lb = min(lb, peak_min);

        while(la <= lb) {
            int mid = (la + lb) / 2;
            int x = mountainArr.get(mid);
            if(x == target)
                return mid;
            if(x > target)
                lb = mid - 1;
            else
                la = mid + 1;
        }

        if(peak_max == n - 2) {
            if(mountainArr.get(n - 1) == target)
                return n - 1;
            else
                return -1;
        }

        ra = max(ra, peak_max);
        while(ra <= rb) {
            int mid = (ra + rb) / 2;
            int x = mountainArr.get(mid);
            if(x == target)
                return mid;
            if(x < target)
                rb = mid - 1;
            else
                ra = mid + 1;
        }

        return -1;
    }
};
