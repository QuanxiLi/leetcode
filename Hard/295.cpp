#include <forward_list>
#include <climits>

constexpr int min_group_size = 8;
constexpr int max_group_size = min_group_size * 2;

class MedianFinder {
public:
    struct LeafNode {
        bool is_leaf;
        int size;
        std::forward_list<int> data;
    };

    struct NonLeafNode {
        bool is_leaf;
        int size;
        int child_count;
        std::forward_list<std::pair<int/*start*/, void *>> data;
    };

    static inline LeafNode *NewLeafNode(int start) {
        LeafNode *leaf = new LeafNode;
        leaf->is_leaf = true;
        leaf->size = 1;
        leaf->data.push_front(start);
        return leaf;
    }

    static inline NonLeafNode *NewNonLeafNode(int start) {
        NonLeafNode *node = new NonLeafNode;
        node->is_leaf = false;
        node->size = 1;
        node->child_count = 1;
        LeafNode *leaf = NewLeafNode(start);
        node->data.emplace_front(start, leaf);
        return node;
    }

    static inline NonLeafNode *CreateParentAndSplit(NonLeafNode *root) {
        NonLeafNode *parent = new NonLeafNode;
        parent->is_leaf = false;
        parent->size = root->size;
        parent->child_count = 2;
        auto another = split(root);
        int afront_ = another->data.front().first;
        parent->data.emplace_front(afront_, another);
        parent->data.emplace_front(root->data.front().first, root);
        return parent;
    }

    // return if data is too much
    static inline bool addNum(int num, LeafNode *node) {
        for(auto it = node->data.before_begin(); ; ) {
            auto next = it;
            next++;
            if(next == node->data.end() || *next >= num ) {
                node->data.insert_after(it, num);
                break;
            }
            it = next;
        }
        node->size++;
        return node->size >= max_group_size;
    }

    static inline void _setSplittedSize(LeafNode *node, LeafNode *another) {
        another->size = node->size - min_group_size;
        node->size = min_group_size;
    }

    static inline int _getSize(NonLeafNode *node) {
        int size = 0;
        for(const auto &child : node->data) {
            size += static_cast<LeafNode *>(child.second)->size;
        }
        return size;
    }

    static inline void _setSplittedSize(NonLeafNode *node, NonLeafNode *another) {
        another->child_count = node->child_count - min_group_size;
        node->child_count = min_group_size;
        another->size = _getSize(another);
        node->size = _getSize(node);
    }

    // split node to node itself and the returned node
    template<typename Node>
    static inline Node *split(Node *node) {
        Node *another = new Node;
        another->is_leaf = node->is_leaf;
        auto node_it = node->data.begin();
        for(int i = 1; i < min_group_size; i++) {
            node_it++;
        }
        another->data.splice_after(another->data.before_begin(), node->data, node_it, node->data.end());
        _setSplittedSize(node, another);
        return another;
    }

    // return if data is too much
    static bool addNum(int num, NonLeafNode *node) {
        auto it = node->data.before_begin();
        while(true) {
            auto next = it;
            next++;
            if(next == node->data.end()) { break; }
            if(next->first > num) { break; }
            it = next;
        }
        void *child = it->second;

        bool need_split = false;
        if(static_cast<LeafNode *>(child)->is_leaf) {
            auto child_ = static_cast<LeafNode *>(child);
            if(addNum(num, child_)) {
                auto another = split(child_);
                int afront_ = another->data.front();
                node->data.emplace_after(it, afront_, another);
                node->child_count++;
                need_split = node->child_count >= max_group_size;
            }
        } else {
            auto child_ = static_cast<NonLeafNode *>(child);
            if(addNum(num, child_)) {
                auto another = split(child_);
                int afront_ = another->data.front().first;
                node->data.emplace_after(it, afront_, another);
                node->child_count++;
                need_split = node->child_count >= max_group_size;
            }
        }
        node->size++;
        return need_split;
    }

    static inline int getKthNum(int k, LeafNode *node) {
        auto it = node->data.begin();
        for(int i = 0; i < k; i++) {
            it++;
        }
        return *it;
    }

    static int getKthNum(int k, NonLeafNode *node) {
        for(auto it = node->data.begin(); /* it != node->data.end() */; it++) {
            void *child = it->second;
            int size = static_cast<LeafNode *>(child)->size;
            if(size > k) {
                if(static_cast<LeafNode *>(child)->is_leaf) {
                    return getKthNum(k, static_cast<LeafNode *>(child));
                } else {
                    return getKthNum(k, static_cast<NonLeafNode *>(child));
                }
            }
            k -= size;
        }
        // error
        return 0;
    }

    NonLeafNode *root;

public:
    /** initialize your data structure here. */
    MedianFinder() {
        root = NewNonLeafNode(INT_MIN);
    }
    
    void addNum(int num) {
        if(addNum(num, root)) {
            root = CreateParentAndSplit(root);
        }
    }
    
    double findMedian() {
        int size = root->size - 1;
        if(size <= 0)
            return 0;
        int a = size / 2 + 1;
        if(size % 2) {
            return getKthNum(a, root);
        } else {
            return ((double)getKthNum(a - 1, root) + (double)getKthNum(a, root)) / 2;
        }
    }
};

/**
 * Your MedianFinder object will be instantiated and called as such:
 * MedianFinder* obj = new MedianFinder();
 * obj->addNum(num);
 * double param_2 = obj->findMedian();
 */
