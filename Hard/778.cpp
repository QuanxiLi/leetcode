#include <set>
#include <vector>
#include <map>
#include <utility>


using namespace std;

class Solution {
    inline static int getParent(int x, int *parent) {
        int p = x;
        while(parent[p] != p) {
            p = parent[p];
        }
        while(parent[x] != p) {
            int t = x;
            x = parent[x];
            parent[t] = p;
        }
        return p;
    }

    inline static void tryUnion(int *parent, pair<uint8_t, uint8_t> pos1,
                                pair<uint8_t, uint8_t> pos2,
                                int height, int n, const vector<vector<int>> &grid
                                ) {
        if(pos2.first < 0 || pos2.first >= n)
            return;
        if(pos2.second < 0 || pos2.second >= n)
            return;
        if(grid[pos2.first][pos2.second] > height)
            return;
        int p1 = pos1.first * n + pos1.second;
        int p2 = pos2.first * n + pos2.second;
        int p = getParent(p1, parent);
        while(parent[p2] != p) {
            int t = p2;
            p2 = parent[p2];
            parent[t] = p;
        }
    }
public:
    int swimInWater(vector<vector<int>>& grid) {
        int n = grid.size();
        int n2 = n * n;
        
        map<int, set<pair<uint8_t, uint8_t>>> grids;
        for(int i = 0; i < n; i++) {
            auto &line = grid[i];
            for(int j = 0; j < n; j++) {
                int h = line[j];
                grids[h].emplace(i, j);
            }
        }

        int *parent = new int[n2];
        for(int i = 0; i < n2; i++) {
            parent[i] = i;
        }
        for(auto &kv : grids) {
            int height = kv.first;
            for(auto pos : kv.second) {
				tryUnion(parent, pos, make_pair(pos.first - 1, pos.second), height, n, grid);
				tryUnion(parent, pos, make_pair(pos.first + 1, pos.second), height, n, grid);
				tryUnion(parent, pos, make_pair(pos.first, pos.second - 1), height, n, grid);
				tryUnion(parent, pos, make_pair(pos.first, pos.second + 1), height, n, grid);
            }
            if(getParent(0, parent) == getParent(n2 - 1, parent))
                return height;
        }
        // impossible to get here
        return 0;
    }
};
