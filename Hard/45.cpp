#include <vector>

using namespace std;

class Solution {
public:
    int jump(vector<int>& nums) {
        int size = nums.size();
        int max = 0;
        for(int i = 0; i < size; i++) {
            int cmax = nums[i] + i;
            if(cmax > max) { max = cmax; }
            nums[i] = max;
        }
        int current = 0;
        int count = 0;
        int last = size - 1;
        while(current < last) {
            current = nums[current];
            count++;
        }
        return count;
    }
};
