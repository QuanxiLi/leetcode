#include <queue>
#include <stack>
#include <vector>
#include <algorithm>

using namespace std;

constexpr int N = 1000000007;

class Solution {
public:
    vector<int> numsGame(vector<int>& nums) {
        vector<int> result;
        int nums_size = nums.size();
        result.reserve(nums_size);
        // 快速求中位数和左右和
        priority_queue<int, vector<int>, greater<int>> large_part;
        priority_queue<int, vector<int>, less<int>> small_part;
        int middle = 0;
        int sums = 0, suml = 0;
        // int n;  // for each half part

        for(int i = 0; i < nums_size; i++) {
            int x = (nums[i] - i) % N;
            if(i & 1) {
                int bigger = max(middle, x);
                int smaller = min(middle, x);
                large_part.push(bigger);
                small_part.push(smaller);
                suml += bigger;
                sums += smaller;
                if(suml < 0)
                    suml += N;
                else if(suml > N)
                    suml -= N;
                if(sums < 0)
                    sums += N;
                else if(sums > N)
                    sums -= N;                
            } else {
                if(x > middle) {
                    large_part.push(x);
                    middle = large_part.top();
                    large_part.pop();
                    suml += x - middle;
                    if(suml < 0)
                        suml += N;
                    else if(suml > N)
                        suml -= N;                
                } else {
                    small_part.push(x);
                    middle = small_part.top();
                    small_part.pop();
                    sums += N - (middle - x);
                    if(sums < 0)
                        sums += N;
                    else if(sums > N)
                        sums -= N;
                    }
            }
            // int count = middle * n - sums + suml - middle * n
            int count = suml - sums;
            if(count < 0)
                count += N;
            result.push_back(count);
        }
        return result;
    }
};
