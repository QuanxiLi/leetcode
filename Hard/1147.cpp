#include <string>

using namespace std;

class Solution {
public:
    inline bool find(const char *&start, const char *&end) {
        const char *mid = start + (end - start + 1) / 2;
        for(const char *find_c = end - 1; find_c >= mid; find_c--) {
            const char *i = start, *j = find_c;
            while(true) {
                if(*i != *j) { break; }
                i++;
                j++;
                if(j == end) {
                    // found
                    start = i;
                    end = find_c;
                    return true;
                }
            }
        }
        return false;
    }

    int longestDecomposition(string text) {
        const char *start = text.c_str();
        const char *end = start + text.size();
        int count = 0;
        while(find(start, end)) {
            count += 2;
            if(start == end) {
                return count;
            }
        }
        return count + 1;
    }
};
