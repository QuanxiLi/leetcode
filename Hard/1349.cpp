#include <vector>
#include <cstring>
#include <algorithm>

using namespace std;

class Solution {
public:
    int maxStudents(vector<vector<char>>& seats) {
        int m = seats.size();
        int n = seats[0].size();
        int s = 1 << n;
        int *dp = new int[s];
        int *new_dp = new int[s];
        memset(dp, 0, s * sizeof(int));
        for(int i = 0; i < m; i++) {
            int mask = 0;
            for(auto c : seats[i]) {
                mask = mask << 1 | (c == '#' ? 1 : 0);
            }
            for(int j = 0; j < s; j++) {
                if((j & mask) || (j & (j << 1))) {
                    new_dp[j] = 0;
                    continue;
                }
                int x = 0;
                for(int k = 0; k < s; k++) {
                    if(j & (k << 1) || j & (k >> 1))
                        continue;
                    x = max(x, dp[k]);
                }
                int jj = j;
                while(jj) {
                    x += jj & 1;
                    jj >>= 1;
                }
                new_dp[j] = x;
            }
            swap(dp, new_dp);
        }
        int x = 0;
        for(int j = 0; j < s; j++) {
            x = max(x, dp[j]);
        }
        delete[] dp;
        delete[] new_dp;
        return x;
    }
};
