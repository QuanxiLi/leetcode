#include <algorithm>
#include <cstring>

using namespace std;

constexpr unsigned N = 1000000007;

class Solution {
public:
    int numWays(int steps, int arrLen) {
        if (arrLen <= 0)
            return 0;
        unsigned half_step = steps / 2;
        unsigned n = min(half_step + 1, static_cast<unsigned>(arrLen));
        unsigned *current = new unsigned[n + 1];
        unsigned *next = new unsigned[n + 1];
        memset(current, 0, sizeof(unsigned) * (n + 1));
        memset(next, 0, sizeof(unsigned) * (n + 1));
        current[0] = 1;

        for (unsigned i = 0; i <= half_step; i++) {
            next[0] = current[0] + current[1];
            if (next[0] > N)
                next[0] -= N;
            for (unsigned j = 1; j <= i + 1 && j < n; j++) {
                next[j] = (current[j] + current[j - 1] + current[j + 1]) % N;
            }
            swap(current, next);
        }

        for (unsigned i = half_step + 1; i < steps; i++) {
            next[0] = current[0] + current[1];
            if (next[0] > N)
                next[0] -= N;
            for (unsigned j = 1; j < steps - i && j < n; j++) {
                next[j] = (current[j] + current[j - 1] + current[j + 1]) % N;
            }
            swap(current, next);
        }

        int result = current[0];

        delete[] current;
        delete[] next;

        return result;
    }
};
