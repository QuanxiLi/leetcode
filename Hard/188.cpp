#include <stack>
#include <string>
using namespace std;

class Solution {
    inline static void skipBlanks(const char *&s) {
        while (*s == ' ')
            s++;
    }

    static inline bool tryFetchInt(const char *&s, int &v) {
        char c = *s;
        if (c < '0' || c > '9')
            return false;
        int value = c - '0';
        s++;
        while (true) {
            char c = *s;
            if (c < '0' || c > '9') {
                break;
            }
            value = value * 10 + (c - '0');
            s++;
        }
        v = value;
        return true;
    }

    enum TokenType {
        Int,
        Bra,
        Ket,
        Plus,
        Minus,
        End
    };

    static inline TokenType tryFetchToken(const char *&s, int &v) {
        skipBlanks(s);
        if (tryFetchInt(s, v)) {
            return TokenType::Int;
        }
        switch (*(s++)) {
        case '(':
            return TokenType::Bra;
        case ')':
            return TokenType::Ket;
        case '+':
            return TokenType::Plus;
        case '-':
            return TokenType::Minus;
        default:
            return TokenType::End;
        }
    }

    static int eval(const char *s) {
        int value;
        stack<TokenType> tokens;
        stack<int> values;
        tokens.push(TokenType::Bra);
        while (true) {
            auto ty = tryFetchToken(s, value);
            switch (ty) {
            case TokenType::Plus:
                break;
            case TokenType::Minus:
                if (tokens.top() == TokenType::Minus) {
                    tokens.pop();
                } else {
                    tokens.push(TokenType::Minus);
                }
                break;
            case TokenType::Bra:
                tokens.push(TokenType::Bra);
                break;
            case TokenType::Ket:
                // ****** ( int
                tokens.pop();
                tokens.pop();
                value = values.top();
                values.pop();
                // don't break
            case TokenType::Int:
                if (tokens.top() == TokenType::Minus) {
                    value = -value;
                    tokens.pop();
                }
                if (tokens.top() == TokenType::Int) {
                    values.top() += value;
                } else {
                    values.push(value);
                    tokens.push(TokenType::Int);
                }
                break;
            case TokenType::End:
                return values.top();
            }
        }
        return 0;
    }

public:
    int calculate(string s) {
        return eval(s.c_str());
    }
};
