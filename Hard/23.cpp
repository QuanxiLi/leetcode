#include <vector>
#include <queue>

using namespace std;

/**
 * Definition for singly-linked list.
 * struct ListNode {
 *     int val;
 *     ListNode *next;
 *     ListNode() : val(0), next(nullptr) {}
 *     ListNode(int x) : val(x), next(nullptr) {}
 *     ListNode(int x, ListNode *next) : val(x), next(next) {}
 * };
 */
class Solution {
public:
    ListNode* mergeKLists(vector<ListNode*>& lists) {
        auto comp = [](ListNode *a, ListNode *b) {
            return a->val > b->val;
        };
        priority_queue<ListNode *, std::vector<ListNode *>, decltype(comp)>
            q(comp);
        for(ListNode *node : lists) {
            if(node)
                q.push(node);
        }
        if(q.empty())
            return nullptr;
        ListNode *head = q.top();
        ListNode *current = head;
        while(true) {
            q.pop();
            ListNode *next = current->next;
            if(next)
                q.push(next);
            if(q.empty())
                break;
            current->next = q.top();
            current = current->next;
        }
        return head;
    }
};
