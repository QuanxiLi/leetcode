#include <vector>
#include <cstring>
#include <set>

using std::vector;

class Solution {
public:
    vector<int> findRedundantDirectedConnection(vector<vector<int>>& edges) {
        int n = edges.size();
        int *parent = new int[n + 1];
        memset(parent, 0, (n + 1) * sizeof(int));

        std::set<int> roots;

        int error_from = 0, error_to = 0;

        for(auto &edge : edges) {
            int from = edge[0];
            int to = edge[1];
            
            if(parent[to] != 0) {
                // one can't be child of two parents
                if(error_to != 0) {
                    return {parent[to], to};
                }
                error_to = to;
                error_from = from;
                continue;
            }

            if(roots.find(to) != roots.end()) {
                // one can't point to its root
                for(int i = from; i != 0; i = parent[i]) {
                    if(i == to) {
                        if(error_to != 0) {
                            return {parent[error_to], error_to};
                        }
                        error_to = to;
                        error_from = from;
                        continue;
                    }
                }
                roots.erase(to);
            }

            parent[to] = from;
            if(parent[from] == 0) {
                roots.insert(from);
            }
        }
        
        return {error_from, error_to};
    }
};
