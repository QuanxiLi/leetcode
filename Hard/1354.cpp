#include <queue>

using namespace std;

class Solution {
public:
    bool isPossible(vector<int>& target) {
        if(target.empty()) {
            return true;
        }
        if(target.size() == 1) {
            return target[0] == 1;
        }
        int max_element = 1;
        for(int i : target) {
            max_element = i > max_element ? i : max_element;
        }
        int max_sum = max_element * 2 - 1;
        priority_queue<int> q;
        unsigned sum = 0;
        for(int i : target) {
            q.push(i);
            sum += i;
            if(sum > max_sum) {
                return false;
            }
        }
        while(true) {
            int t = q.top();
            if(t == 1) {
                return true;
            }
            q.pop();
            int t2 = q.top();
            int sum_left = sum - t;
            t = t2 + (t - t2) % sum_left;
            if(t == 1) {
                return true;
            }
            t -= sum_left;
            sum = sum_left + t;
            if(t <= 0) {
                return false;
            }
            q.push(t);
        }
    }
};
