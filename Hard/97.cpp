#include <stack>
#include <string>
#include <utility>
#include <vector>

using namespace std;

class Solution {
    inline bool eqfrom(string &sa, string &sb, int i, int j) {
        for (; i < sa.size(); i++, j++) {
            if (sa[i] != sb[j]) {
                return false;
            }
        }
        return true;
    }

public:
    bool isInterleave(string s1, string s2, string s3) {
        vector<vector<bool>> searched;
        for (int i = 0; i < s1.size() + 1; i++) {
            searched.emplace_back(s2.size() + 1, false);
        }
        if (s1.size() + s2.size() != s3.size()) {
            return false;
        }
        stack<pair<int, int>> states;
        states.emplace(0, 0);
        while (!states.empty()) {
            auto s = states.top();
            states.pop();
            searched[s.first][s.second] = true;
            if (s.first == s1.size()) {
                if (eqfrom(s2, s3, s.second, s.first + s.second)) {
                    return true;
                } else {
                    continue;
                }
            }
            if (s.second == s2.size()) {
                if (eqfrom(s1, s3, s.first, s.first + s.second)) {
                    return true;
                } else {
                    continue;
                }
            }
            char c1 = s1[s.first];
            char c2 = s2[s.second];
            int s3index = s.first + s.second;
            char c3 = s3[s3index];
            if (c1 == c3) {
                if (!searched[s.first + 1][s.second]) {
                    states.emplace(s.first + 1, s.second);
                }
            }
            if (c2 == c3) {
                if (!searched[s.first][s.second + 1]) {
                    states.emplace(s.first, s.second + 1);
                }
            }
        }
        return false;
    }
};
