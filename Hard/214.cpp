#include <string>

using namespace std;

class Solution {
    inline static string generate(
        const char *a, const char *b, const char *end) {
        string result;
        result.reserve((end - a) + (end - b));
        for(const char *p = end - 1; p >= b; p--) {
            result.push_back(*p);
        }
        for(const char *p = a; p < end; p++) {
            result.push_back(*p);
        }
        return result;
    }
public:
    string shortestPalindrome(string s) {
        // 分解 s = a b ，其中 a 是回文，即 a = ~a
        // 寻找使得 | a | 最大的分解方式，
        // 然后返回 ~b a b
        const char *beg = s.c_str();
        const char *end = beg + s.size();
        for(const char *e = end - 1; e >= beg; e--) {
            // 判断[beg, e]是否为回文
            const char *i = beg;
            const char *j = e;
            while(true) {
                if(i >= j) {
                    return generate(beg, e + 1, end);
                }
                if(*i != *j) {
                    break;
                }
                i++;
                j--;
            }
        }
        return "";
    }
};

