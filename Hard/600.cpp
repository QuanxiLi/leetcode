class Solution {
public:
    int findIntegers(int num) {
        int a = 1;
        int b = 0;
        bool x = true;
        for (int i = 31; i >= 0; i--) {
            int aa = a + b;
            if (!((num >> i) & 3) && x)
                b = a - 1;
            else
                b = a;
            if (((num >> i) & 3) == 3) {
                x = false;
            }
            a = aa;
        }
        return a + b;
    }
};
