#include <vector>
#include <set>
#include <tuple>

using namespace std;

class Solution {
public:
	vector<double> medianSlidingWindow(vector<int>& nums, int k) {
		vector<double> result;
		set<tuple<int, int>> window;
		for (int i = 0; i < k; i++) {
			window.emplace(nums[i], i);
		}
		auto it = window.begin();
		int n = nums.size();
		int p = (k - 1) / 2;
		for (int i = 0; i < p; i++) {
			it++;
		}
		int x = get<0>(*it);
		if (k % 2 == 0) {
			auto nxt_it = it;
			nxt_it++;
			result.push_back((x + static_cast<double>(get<0>(*nxt_it))) / 2);
		}
		else {
			result.push_back(x);
		}
		for (int i = k; i < n; i++) {
			int j = i - k;
			int a = nums[i];
			int b = nums[j];
			if (b == x && j == get<1>(*it)) {
				window.emplace(a, i);
				auto last_it = it;
				if (a >= x) {
					x = get<0>(*++it);
				}
				else {
					x = get<0>(*--it);
				}
				window.erase(last_it);
			}
			else {
				window.erase(tuple<int, int>(b, j));
				window.emplace(a, i);
				if (a >= x && b <= x) {
					x = get<0>(*++it);
				}
				else if (a < x && b > x) {
					x = get<0>(*--it);
				}
			}
			if (k % 2 == 0) {
				auto nxt_it = it;
				nxt_it++;
				result.push_back((x + static_cast<double>(get<0>(*nxt_it))) / 2);
			}
			else {
				result.push_back(x);
			}
		}
		return result;
	}
};
