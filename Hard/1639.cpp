#include <tuple>
#include <algorithm>

using namespace std;

/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode() : val(0), left(nullptr), right(nullptr) {}
 *     TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
 *     TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
 * };
 */
class Solution {
    tuple<int/*max sum*/, bool/*is bst*/, int/*sum*/, int/*max val*/, int/*min val*/>
    maxSumBSTHelp(TreeNode* node) {
        if(node == nullptr) {
            return make_tuple(0, true, 0, 0, 0);
        }
        auto [msl, ibl, sl, maxl, minl] = maxSumBSTHelp(node->left);
        auto [msr, ibr, sr, maxr, minr] = maxSumBSTHelp(node->right);
        bool ib = ibl && ibr && 
            (node->left == nullptr || maxl < node->val) &&
            (node->right == nullptr || minr > node->val);
        int s = ib ? sl + sr + node->val : 0;
        int ms = max({msl, msr, s});
        int maxv = node->right ? maxr : node->val;
        int minv = node->left ? minl : node->val;
        return make_tuple(ms, ib, s, maxv, minv);
    }
public:
    int maxSumBST(TreeNode* root) {
        auto [ms, ib, s, maxv, minv] = maxSumBSTHelp(root);
        return ms;
    }
};
