class Solution {
public:
    int minPatches(vector<int> &nums, int n) {
        int count = 0;
        int i = 0;
        if (nums.size() <= 0 || nums[0] != 1) {
            count++;
        } else {
            i++;
        }
        unsigned limit = 1;
        while (limit < n) {
            if (nums.size() <= i || nums[i] > limit + 1) {
                count++;
                limit += limit + 1;
            } else {
                limit += nums[i];
                i++;
            }
        }
        return count;
    }
};
