#include <algorithm>
#include <vector>

using namespace std;

class Solution {
    int *data;
    int n;

    int findMoreThan(int val, int j, int i) {
        for (; j < i; j++) {
            if (data[j] > val) {
                break;
            }
        }
        return j;
    }

    // count of pairs with less distance than `dis`
    // O(n) time
    int countNearPairs(int dis) {
        int count = 0;
        int j = 0;
        for (int i = 0; i < n; i++) {
            j = findMoreThan(data[i] - dis, j, i);
            count += i - j;
        }
        return count;
    }

public:
    int smallestDistancePair(vector<int> &nums, int k) {
        sort(nums.begin(), nums.end());
        data = nums.data();
        n = nums.size();
        int min_dist = 0, max_dist = data[n - 1] - data[0];
        while (min_dist < max_dist) {
            int mid_dist = (min_dist + max_dist + 1) / 2;
            int count = countNearPairs(mid_dist);
            if (count < k) {
                min_dist = mid_dist;
            } else {
                max_dist = mid_dist - 1;
            }
        }
        return min_dist;
    }
};
