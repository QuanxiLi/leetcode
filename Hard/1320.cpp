#include <string>
#include <cmath>

using namespace std;

#define GET_X(C)        (((C) - 'A') % 6)
#define GET_Y(C)        (((C) - 'A') / 6)
#define DISTANCE(I, J)  (std::abs(GET_X(word[I]) - GET_X(word[J])) + std::abs(GET_Y(word[I]) - GET_Y(word[J])))

class Solution {
public:
    int minimumDistance(string word) {
        /*
        dp[i][j]: min distance for starting from i & j, i < j, and type word[i:]
        dp[n-1][n] = 0
        dp[i-1][j] = distance(word[i-1], word[i]) + dp[i][j]    if j > i
        dp[i-1][i] = MIN(distance(word[i-1], word[j]) + dp[i][j] for j = i+1 ... n)

        */
        int n = word.size();
        if(n <= 2) {
            return 0;
        }
        int *dpi = new int[n + 1];
        int *dpi_1 = new int[n + 1];

        dpi_1[n - 1] = 0;
        dpi_1[n] = DISTANCE(n - 2, n - 1);
        for(int i = n - 2; i > 0; i--) {
            int d = DISTANCE(i - 1, i);
            int min_dist = INT_MAX;
            swap(dpi, dpi_1);
            for(int j = i + 1; j < n; j++) {
                dpi_1[j] = dpi[j] + d;
                int dist = dpi[j] + DISTANCE(i - 1, j);
                if(min_dist > dist)
                    min_dist = dist;
            }
            dpi_1[n] = dpi[n] + d;
            dpi_1[i] = min(min_dist, dpi[n]);
        }

        int md = INT_MAX;
        for(int j = 1; j < n; j++) {
            int dp0j = dpi_1[j];
            if(dp0j < md)
                md = dp0j;
        }

        delete[] dpi;
        delete[] dpi_1;

        return md;
    }
};
