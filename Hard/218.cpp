#include <vector>
#include <algorithm>
#include <set>

using namespace std;

class Solution {
public:
    vector<vector<int>> getSkyline(vector<vector<int>>& buildings) {
        int count = buildings.size();
        int* start = new int[count];
        int* end = new int[count];
        for(int i = 0; i < count; i++) {
            start[i] = i;
            end[i] = i;
        }
        sort(start, start + count, [&](int a, int b) {
            return buildings[a][0] < buildings[b][0];
        });
        sort(end, end + count, [&](int a, int b) {
            return buildings[a][1] < buildings[b][1];
        });
        
        vector<vector<int>> result;
        auto hcomp = [&](int a, int b) {
            int ha = buildings[a][2];
            int hb = buildings[b][2];
            if(ha == hb) {
                return buildings[a][1] > buildings[b][1];
            } else {
                return ha > hb;
            }
        };
        set<int, decltype(hcomp)> current_building(hcomp);
        int si = 0, ei = 0;
        int last_height = -1;
        while(si < count || ei < count) {
            int x;
            if(si >= count) {
                x = buildings[end[ei]][1];
                while(true) {
                    current_building.erase(end[ei]);
                    ei++;
                    if(ei >= count || buildings[end[ei]][1] > x) {
                        break;
                    }
                }
            } else if(ei >= count) {
                x = buildings[start[si]][0];
                while(true) {
                    current_building.insert(start[si]);
                    si++;
                    if(si >= count || buildings[start[si]][1] > x) {
                        break;
                    }
                }
            } else {
                int xs = buildings[start[si]][0];
                int xe = buildings[end[ei]][1];
                x = min(xs, xe);
                while(buildings[end[ei]][1] <= x) {
                    current_building.erase(end[ei]);
                    ei++;
                    if(ei >= count) {
                        break;
                    }
                }
                while(buildings[start[si]][0] <= x) {
                    current_building.insert(start[si]);
                    si++;
                    if(si >= count) {
                        break;
                    }
                }
            }
            int current_height;
            if(!current_building.empty()) {
                current_height = buildings[*current_building.begin()][2];
            } else {
                current_height = 0;
            }
            if(current_height != last_height) {
                result.push_back(vector<int>{ x, current_height });
                last_height = current_height;
            }
        }

        delete[] end;
        delete[] start;
        return result;
    }
};
