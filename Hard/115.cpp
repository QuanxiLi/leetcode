#include <string>
#include <cstring>
#include <algorithm>
#include <climits>

using namespace std;

class Solution {
public:
    int numDistinct(string s, string t) {
        // dp[i][j] = dp[i-1][j] + dp[i-1][j-1] if s[i] == t[j]
        //          = dp[i-1][j]                otherwise
        // dp[-1][j] = 0
        // dp[i][-1] = 1
        int sn = s.size();
        int tn = t.size();
        int *dp = new int[tn + 1] + 1;
        int *dp_last = new int[tn + 1] + 1;
        dp[-1] = 1;
        dp_last[-1] = 1;
        memset(dp_last, 0, sizeof(int) * tn);
        for(int i = 0; i < sn; i++) {
            int si = s[i];
            for(int j = 0; j < tn; j++) {
                if(si == t[j]) {
                    unsigned temp = (unsigned)dp_last[j] + (unsigned)dp_last[j - 1];
                    dp[j] = temp & INT_MAX;
                } else {
                    dp[j] = dp_last[j];
                }
            }
            swap(dp, dp_last);
        }
        int result = dp_last[tn - 1];
        delete[] (dp - 1);
        delete[] (dp_last - 1);
        return result;
    }
};
