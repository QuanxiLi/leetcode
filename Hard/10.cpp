#include <string>
#include <vector>
#include <utility>
#include <stack>

using namespace std;

class Solution {
    struct Term {
        char c;
        bool star;
    };

    struct Status {
        vector<Term>::iterator p_it;
        string::iterator s_it;

        Status(vector<Term> &terms, string &s)
            : p_it(terms.begin()), s_it(s.begin())
            {}

        Status(const Status &other)
            : p_it(other.p_it), s_it(other.s_it)
            {}
    };
public:
    bool isMatch(string s, string p) {
        s.push_back('$');
        p.push_back('$');
        vector<Term> terms;
        Term current;
        const char *pattern = p.c_str();
        for(const char *ptr = pattern; *ptr; ptr++) {
            current.c = *ptr;
            if(*(ptr + 1) == '*') {
                ptr++;
                current.star = true;
            } else {
                current.star = false;
            }
            if(!terms.empty()) {
                Term &last = terms.back();
                if(last.c == current.c && last.star) {
                    if(!current.star) {
                        last.star = false;
                        current.star = true;
                    } else {
                        continue;
                    }
                }
            }
            terms.push_back(current);
        }



        Status status(terms, s);
        stack<Status> other_status;
       
        while(true) {
            // check if success / failed
            bool _s_end = status.s_it == s.end();
            bool _p_end = status.p_it == terms.end();
            if(_p_end && _s_end) { return true; }
            if(_s_end) {
                while(status.p_it != terms.end()) {
                    if(status.p_it->c != '.')
                        goto GET_OTHER_STATUS;
                    status.p_it++;
                }
                return true;
            }
            if(_p_end || _s_end) {
            GET_OTHER_STATUS:
                if(other_status.empty()) {
                    return false;
                } else {
                    status = other_status.top();
                    other_status.pop();
                }
                continue;
            }
            // go one step
            if(status.p_it->star) {
                // i will skip while `other` will stick
                if(status.p_it->c == '.' || status.p_it->c == *(status.s_it)) {
                    other_status.emplace(status);
                    other_status.top().s_it++;
                }
                // skip it
                status.p_it++;
                continue;
            }
            if(status.p_it->c != '.' && status.p_it->c != *(status.s_it)) {
                goto GET_OTHER_STATUS;
            }
            status.p_it++;
            status.s_it++;
        }

        return false;
    }
};
