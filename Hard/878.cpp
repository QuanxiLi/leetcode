#include <algorithm>

constexpr int BIGNUM = 1000000007;

class Solution {
    static inline unsigned lcm(unsigned a, unsigned b) {
        if (a < b) {
            std::swap(a, b);
        }
        int x = a * b;
        while (b != 0) {
            int t = a % b;
            a = b;
            b = t;
        }
        return x / a;
    }

public:
    int nthMagicalNumber(int N, int A, int B) {
        unsigned C = lcm(A, B);
        unsigned aa = C / A;
        unsigned bb = C / B;
        // k * C 内有k * (aa + bb - 1)个
        unsigned cc = aa + bb - 1;
        unsigned k = N / cc;
        unsigned nn = N % cc; // nn < 8e4
        // 找到第nn个，其中包括ma个A的倍数
        // ma * A = (nn - ma) * B
        // nn * B < 32e8
        unsigned ma = nn * static_cast<unsigned>(B) / static_cast<unsigned>(A + B);
        unsigned mb = nn - ma;
        // ma 偏小，ma * A <= (nn - ma) * B
        unsigned x;
        unsigned aaa = (ma + 1) * A;
        unsigned bbb = mb * B;
        if (aaa > bbb) {
            x = bbb;
        } else {
            x = aaa;
        }
        // 第nn个是x
        return (static_cast<uint64_t>(k) * C + x) % BIGNUM;
    }
};
