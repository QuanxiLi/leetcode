#include <cstdint>
#include <vector>

using namespace std;

#define NEIGHBOR(X) ((X) ^ 1)
#define MARK(X)     (mark[(X) >> 1] = 1)
#define MARKED(X)   (mark[(X) >> 1])

class Solution {
public:
    int minSwapsCouples(vector<int>& row) {
        int size = row.size();
        int half_size = size / 2;
        if(size == 0) { return 0; }
        int8_t *location = new int8_t[size];
        int8_t *mark = new int8_t[half_size];
        for(int i = 0; i < size; i++) {
            location[row[i]] = i;
        }
        for(int i = 0; i < half_size; i++) {
            mark[i] = 0;
        }
        int result = 0;
        int start = 0;
        while(true) {
            int current_loc = start;
            while(true) {
                int current = row[current_loc];
                MARK(current);
                int lover = NEIGHBOR(current);
                int lover_loc = location[lover];
                current_loc = NEIGHBOR(lover_loc);
                if(current_loc == start) { break; }
                result++;
            }
            while(MARKED(row[start])) {
                start++;
                if(start >= size)
                    goto END;
            }
        }
    END:
        delete[] location;
        delete[] mark;
        
        return result;
    }
};

