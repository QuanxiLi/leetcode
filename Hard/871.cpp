class Solution {
public:
    int minRefuelStops(int target, int startFuel, vector<vector<int>> &stations) {
        int n = stations.size();
        vector<bool> used(n, false);
        int fuel = startFuel;
        for (int i = 0; i < n; i++) {
            if (fuel >= target)
                return i;
            int max_addition = 0;
            int best_j;
            for (int j = 0; j < n; j++) {
                if (stations[j][0] > fuel)
                    break;
                if (used[j])
                    continue;
                if (stations[j][1] > max_addition) {
                    max_addition = stations[j][1];
                    best_j = j;
                }
            }
            if (max_addition > 0) {
                fuel += max_addition;
                used[best_j] = true;
            }
        }
        if (fuel >= target)
            return n;
        else
            return -1;
    }
};
