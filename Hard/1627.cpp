#include <vector>

using namespace std;

class Solution {
    inline static int getParent(int *parent, int x) {
        int p = parent[x];
        if(parent[p] == p)
            return p;
        do {
            p = parent[p];
        } while(parent[p] != p);
        do {
            int y = parent[x];
            parent[x] = p;
            x = y;
        } while(x != p);
        return p;
    }

    inline static void makeUnion(int *parent, int a, int b) {
        // assert a < b
        int p = getParent(parent, a);
        while(parent[b] != p) {
            int y = parent[b];
            parent[b] = p;
            b = y;
        }
    }

    inline static void checkPrime(vector<int> &primes, int x) {
        for(auto p : primes) {
            if(x % p == 0)
                return;
        }
        primes.push_back(x);
    }

    inline static vector<int> getPrimesUntil(int m) {
        vector<int> primes { 2, 3, 5, 7, 11 };
        for(int k = 12; k < m; k += 6) {
            checkPrime(primes, k + 1);
            checkPrime(primes, k + 5);
        }
        return primes;
    }
public:
    vector<bool> areConnected(int n, int threshold, vector<vector<int>>& queries) {
        if(threshold <= 0) {
            vector<bool> result;
            result.reserve(queries.size());
            for(int i = 0; i < queries.size(); i++) {
                result.push_back(true);
            }
            return result;
        }
        // S = sets of ints where each integer in each set is connected
        // for a new int x:
        //  for each factor f of x, where f > threshold:
        //      if exists m in Si, where m % f == 0 and Si in S, then a in Si
        //      if a in Si and a in Sj, union Si and Sj
        int *parent = new int[n + 1];
        for(int i = 0; i <= n; i++) {
            parent[i] = i;
        }
        vector<int> primes = getPrimesUntil(n / threshold + 1);
        for(int x = threshold + 1; x <= n; x++) {
            for(auto p : primes) {
                int y = x / p;
                if(y <= threshold)
                    break;
                if(x % p == 0) {
                    makeUnion(parent, y, x);
                }
            }
        }
        for(int i = 0; i < n; i++) {
            int p = parent[i];
            int pp = parent[p];
            if(pp != p) {
                parent[i] = pp;
            }
        }
        vector<bool> answer;
        answer.reserve(queries.size());
        for(auto &q : queries) {
            answer.push_back(parent[q[0]] == parent[q[1]]);
        }
        delete[] parent;
        return answer;
    }
};
