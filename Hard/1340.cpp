#include <algorithm>
#include <cmath>
#include <vector>

using namespace std;

class Solution {
public:
	int maxJumps(vector<int>& arr, int d) {
		int n = arr.size();
		int *h_order = new int[n];
		for (int i = 0; i < n; i++) {
			h_order[i] = i;
		}
		sort(h_order, h_order + n, [&](int a, int b) {
			return arr[a] > arr[b];
		});
		int *count = new int[n];
		int max_count = 0;
		for (int i = 0; i < n; i++) {
			int c = 0;
			int hoi = h_order[i];
			int hi = arr[hoi];
			int lb = max(0, hoi - d);
			int ub = min(n - 1, hoi + d);
			for (int j = hoi - 1; j >= lb; j--) {
				if (arr[j] > hi) {
					c = count[j];
					break;
				}
			}
			for (int j = hoi + 1; j <= ub; j++) {
				if (arr[j] > hi) {
					c = max(c, count[j]);
					break;
				}
			}
			count[hoi] = c + 1;
			if (count[hoi] > max_count) {
				max_count = count[hoi];
			}
		}

		delete[] count;
		delete[] h_order;
		return max_count;
	}
};

