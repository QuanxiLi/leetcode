#include <climits>
#include <vector>

using namespace std;

/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode(int x) : val(x), left(NULL), right(NULL) {}
 * };
 */
class Solution {
    int max_count;
    vector<int> answer;
    int current;
    int current_count;

    inline void nextVal() {
        if(current_count > max_count) {
            max_count = current_count;
            answer.clear();
            answer.push_back(current);
        } else if(current_count == max_count) {
            answer.push_back(current);
        }
    }

    inline void submit(int v) {
        if(v > current) {
            nextVal();
            current = v;
            current_count = 1;
        } else {
            current_count++;
        }
    }

    void traverse(TreeNode *node) {
        if(node->left) {
            traverse(node->left);
        }
        submit(node->val);
        if(node->right) {
            traverse(node->right);
        }
    }
public:
    vector<int> findMode(TreeNode* root) {
        if(root == nullptr) {
            return {};
        }
        max_count = 0;
        answer.clear();
        current = INT_MIN;
        current_count = 0;
        traverse(root);
        nextVal();
        return std::move(answer);
    }
};
