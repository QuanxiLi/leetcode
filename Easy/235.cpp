#include <algorithm>

/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode(int x) : val(x), left(NULL), right(NULL) {}
 * };
 */

class Solution {
public:
    TreeNode* lowestCommonAncestor(TreeNode* root, TreeNode* p, TreeNode* q) {
        TreeNode *node = root;
        int pval = p->val;
        int qval = q->val;
        int min_val = std::min(pval, qval);
        int max_val = std::max(pval, qval);
        while(true) {
            int nval = node->val;
            if(nval < min_val) {
                node = node->right;
            } else if(nval > max_val) {
                node = node->left;
            } else {
                return node;
            }
        }
        // error
        return nullptr;
    }
};
