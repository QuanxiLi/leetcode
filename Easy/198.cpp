#include <vector>
#include <algorithm>

using namespace std;

class Solution {
public:
    int rob(vector<int>& nums) {
        if(nums.empty()) {
            return 0;
        }
        auto it = nums.begin();
        int a = 0;
        int b = *it;
        ++it;
        for(; it != nums.end(); it++) {
            int c = max(*it + a, b);
            a = b;
            b = c;
        }
        return b;
    }
};
