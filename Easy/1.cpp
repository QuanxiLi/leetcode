#include <map>

using std::vector;

class Solution {
public:
    vector<int> twoSum(vector<int>& nums, int target) {
        std::map<int, int> left;
        for(int i = 0; i < nums.size(); i++) {
            int x = nums[i];
            auto f = left.find(target - x);
            if(f != left.end()) {
                return {f->second, i};
            }
            left[x] = i;
        }
        return {};
    }
};
