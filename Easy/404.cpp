#include <stack>

using namespace std;

/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode(int x) : val(x), left(NULL), right(NULL) {}
 * };
 */
class Solution {
public:
    int sumOfLeftLeaves(TreeNode* root) {
        stack<TreeNode*> nodes;
        if(root && (root->left || root->right)) {
            nodes.push(root);
        } else {
            return 0;
        }
        int sum = 0;
        while(!nodes.empty()) {
            auto current = nodes.top();
            nodes.pop();
            auto left = current->left;
            if(left) {
                if(left->left || left->right) {
                    nodes.push(left);
                } else {
                    sum += left->val;
                }
            }
            auto right = current->right;
            if(right) {
                if(right->left || right->right) {
                    nodes.push(right);
                }
            }
        }
        return sum;
    }
};
