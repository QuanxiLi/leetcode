#include <string>
#include <vector>

using namespace std;

class Solution {
public:
    bool checkPartitioning(string s) {
        int n = s.size();
        vector<int> starts;
        starts.reserve(n);
        int start_ = 0;
        for (int i = 0; i < n; i++) {
            starts.push_back(start_);
            start_ += n - i;
        }
        vector<bool> palindromes(start_, true);
        // len = 2, 3
        for (int i = 0; i <= n - 3; i++) {
            palindromes[starts[1] + i] = (s[i] == s[i + 1]);
            palindromes[starts[2] + i] = (s[i] == s[i + 2]);
        }
        palindromes[starts[1] + n - 2] = (s[n - 2] == s[n - 1]);
        // len > 3
        for (int len = 4; len <= n - 2; len++) {
            for (int i = 0; i <= n - len; i++) {
                palindromes[starts[len - 1] + i] = 
                    palindromes[starts[len - 3] + i + 1] && s[i] == s[i + len - 1];
            }
        }
        for (int i = 1; i <= n - 2; i++) {
            if (!palindromes[starts[i - 1]])
                continue;
            for (int j = i + 1; j < n; j++) {
                if (palindromes[starts[j - i - 1] + i] &&
                        palindromes[starts[n - j - 1] + j]) {
                    return true;
                }
            }
        }
        return false;
    }
};
