#include <string>
#include <unordered_map>
#include <set>
#include <algorithm>

using namespace std;

class Solution {
public:
	string longestPrefix(string s) {
		unordered_map<int, int> patterns;
		set<int, greater<int>> longest_patterns;
		char first = s[0];
		int n = s.size();
		for (int i = 1; i < n; i++) {
			if (s[i] == first) {
				patterns[i] = 1;
				longest_patterns.insert(i);
			}
		}
		if (longest_patterns.empty()) {
			return "";
		}
		int result_length = s[n - 1] == first ? 1 : 0;
		int idx = 1;
		while (!longest_patterns.empty()) {
			auto it = patterns.find(idx);
			if (it != patterns.end()) {
				int ptl = it->second;
				auto lpit = longest_patterns.begin();
				while (lpit != longest_patterns.end()) {
					auto f = patterns.find(*lpit + idx);
					int next_length = (f != patterns.end()) ? f->second : 0;
					if (next_length <= ptl && *lpit + idx + next_length == n) {
						result_length = idx + next_length;
					}
					patterns[*lpit] = idx + min(ptl, next_length);
					auto erase_it = lpit;
					lpit++;
					if (next_length < ptl) {
						longest_patterns.erase(erase_it);
					}
				}
				idx += ptl;
			}
			else {
				char c = s[idx];
				auto lpit = longest_patterns.begin();
				while (lpit != longest_patterns.end()) {
					int lpi = *lpit;
					auto erase_it = lpit;
					lpit++;
					if (lpi + idx < n && s[lpi + idx] == c) {
						++patterns[lpi];
						if (lpi + idx == n - 1) {
							result_length = idx + 1;
						}
					}
					else {
						longest_patterns.erase(erase_it);
					}
				}
				idx++;
			}   // end if
		}   // end while
		return s.substr(0, result_length);
	}
};
