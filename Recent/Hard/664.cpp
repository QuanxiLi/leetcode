#include <string>
#include <algorithm>
#include <unordered_map>

using namespace std;

class Solution {
public:
    int strangePrinter(string s) {
        if (s.empty()) return 0;
        string str;
        char last = '\0';
        for (char c : s) {
            if (c != last) {
                last = c;
                str.push_back(c);
            }
        }
        int n = str.size();
        int m = n * (n + 1) / 2;
        int *count = new int[m];
        int **start = new int*[n];
        int *start_ = count;
        for (int i = 0; i < n; i++) {
            start[i] = start_;
            start_ += n - i;
        }
        for (int i = 0; i < n; i++) {
            start[i][0] = 1;
        }
        for (int l = 2; l <= n; l++) {
            for (int to = l - 1; to < n; to++) {
                int from = to - l + 1;
                int best;
                if (str[from] == str[to]) {
                    best = start[from + 1][l - 3] + 1;
                    for (int m = from + 2; m < to; m++) {
                        if (str[m] == str[from]) {
                            best = min(best, start[from + 1][m - from - 2]
                                + start[m][l + from - m - 1]);
                        }
                    }
                } else {
                    best = l;
                    for (int i = 0; i < l - 1; i++) {
                        best = min(best, start[from][i] + start[from + i + 1][l - i - 2]);
                    }
                }
                start[from][l - 1] = best;
            }
        }
        int result = start[0][n - 1];
        delete[] start;
        delete[] count;
        return result;
    }
};
