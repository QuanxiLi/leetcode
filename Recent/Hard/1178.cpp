#include <unordered_map>
#include <vector>
#include <string>

using namespace std;

class Solution {
public:
    vector<int> findNumOfValidWords(vector<string>& words, vector<string>& puzzles) {
        unordered_map<int, int> modes;
        for (auto &s : words) {
            int m = 0;
            for (char c : s) {
                m |= (1 << (c - 'a'));
            }
            auto it = modes.find(m);
            if (it != modes.end()) {
                it->second++;
            } else {
                modes[m] = 1;
            }
        }
        vector<int> result;
        result.reserve(puzzles.size());
        for (auto &s : puzzles) {
            int m = 0;
            for (char c : s) {
                m |= (1 << (c - 'a'));
            }
            int m0 = 1 << (s[0] - 'a');
            m ^= m0;
            int m1 = m;
            int count = 0;
            while (true) {
                auto it = modes.find(m | m0);
                if (it != modes.end()) {
                    count += it->second;
                }
                if (m == 0) { break; }
                m = (m - 1) & m1;
            }
            result.push_back(count);
        }
        return result;
    }
};
