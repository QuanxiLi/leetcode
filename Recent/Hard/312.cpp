#include <vector>
#include <algorithm>

using namespace std;

class Solution {
public:
    int maxCoins(vector<int>& nums) {
        int n = nums.size();
        if (n <= 2) {
            if (n == 1) {
                return nums[0];
            } else {
                // n == 2
                int a = nums[0], b = nums[1];
                return a * b + max(a, b);
            }
        }
        int *buffer = new int[n * (n + 1) / 2];
        int **coin = new int*[n];
        int *p = buffer;
        for (int i = 0; i < n; i++) {
            coin[i] = p;
            p += n - i;
        }

        coin[0][0] = nums[0] * nums[1];
        for (int i = 1; i < n - 1; i++) {
            coin[i][0] = nums[i - 1] * nums[i] * nums[i + 1];
        }
        coin[n - 1][0] = nums[n - 1] * nums[n - 2];
        int c;
        for (int l = 2; l <= n; l++) {
            int i = 0, j = l;
            for (; j <= n; i++, j++) {
                int left = (i ? nums[i - 1] : 1);
                int right = (j == n ? 1 : nums[j]);
                int lr = left * right;
                c = max(
                    lr * nums[i] + coin[i + 1][l - 2],
                    lr * nums[j - 1] + coin[i][l - 2]
                );
                for (int k = i + 1; k < j - 1; k++) {
                    c = max(c,
                        lr * nums[k] + coin[i][k - i - 1] + coin[k + 1][j - k - 2]
                    );
                }
                coin[i][l - 1] = c;
            }
        }

        delete [] coin;
        delete [] buffer;
        return c;
    }
};
