#include <string>
#include <vector>
#include <algorithm>

using namespace std;

class Solution {
public:
    int strongPasswordChecker(string password) {
        int n = password.size();
        bool lower = false;
        bool upper = false;
        bool digit = false;
        char last_char = 0;
        int ex1 = 0;
        int ex2 = 0;
        int ex3 = 0;
        int continue_count = 0;
        auto get_ex = [&]() {
            if (continue_count > 2) {
            int ex = continue_count - 2;
                switch (ex % 3) {
                case 1:
                    ex1++;
                    break;
                case 2:
                    ex2++;
                    break;
                }
            ex3 += ex / 3;
            }
            continue_count = 1;
        };
        for (char c : password) {
            lower |= (c >= 'a' && c <= 'z');
            upper |= (c >= 'A' && c <= 'Z');
            digit |= (c >= '0' && c <= '9');
            if (c == last_char) {
                continue_count++;
            } else {
                get_ex();
            }
            last_char = c;
        }
        get_ex();
        
        int k = (lower ? 0 : 1) + (upper ? 0 : 1) + (digit ? 0 : 1);

        if (n < 6) {
            // try to solve all problems by insertion
            // at most one 3+ continuous pattern
            // can be solved by one insertion
            return max(6 - n, k);
        }
        if (n > 20) {
            // need at least (n - 20) delections and k mutations
            int d = n - 20;
            if (ex1 > d) {
                ex1 -= d;
                d = 0;
            } else {
                d -= ex1;
                ex1 = 0;
            }
            if (ex2 > d / 2) {
                ex2 -= d / 2;
                d = 0;
            } else {
                d -= ex2 * 2;
                ex2 = 0;
            }
            if (ex3 > d / 3) {
                ex3 -= d / 3;
                d = 0;
            } else {
                d -= ex3 * 3;
                ex3 = 0;
            }
            int mut = ex1 + ex2 + ex3;
            mut = max(mut, k);
            return n - 20 + mut;
        }
        // 6 <= n <= 20
        return max(ex1 + ex2 + ex3, k);
    }
};
