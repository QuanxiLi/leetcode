#include <string>

using namespace std;

class Solution {
public:
	bool isTransformable(string s, string t) {
		int n = s.size();
		if (n != t.size()) { return false; }
		int index[10];
		for (int i = 0; i < 10; i++) {
			index[i] = n;
		}
		int index_count = 0;
		for (int i = 0; i < n; i++) {
			int x = s[i] - '0';
			if (index[x] == n) {
				index[x] = i;
				index_count++;
				if (index_count == 10) {
					break;
				}
			}
		}
		for (char c : t) {
			int x = c - '0';
			int idx = index[x];
			if (idx == n)
				return false;
			for (int y = 0; y < x; y++) {
				if (index[y] < idx)
					return false;
			}
			for (idx++; idx < n; idx++) {
				if (s[idx] == c)
					break;
			}
			index[x] = idx;
		}
		return true;
	}
};
