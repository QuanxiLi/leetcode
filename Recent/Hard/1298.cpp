#include <vector>
#include <unordered_set>

using namespace std;

class Solution {
public:
    int maxCandies(vector<int>& status, vector<int>& candies, vector<vector<int>>& keys, vector<vector<int>>& containedBoxes, vector<int>& initialBoxes) {
        unordered_set<int> opened;
        unordered_set<int> closed;
        unordered_set<int> unused_keys;
        int total_candies = 0;
        for (int box : initialBoxes) {
            if (status[box]) {
                opened.insert(box);
            } else {
                closed.insert(box);
            }
        }
        while (!opened.empty()) {
            auto it = opened.begin();
            int box = *it;
            opened.erase(it);
            total_candies += candies[box];
            for (int k : keys[box]) {
                auto b_it = closed.find(k);
                if (b_it == closed.end()) {
                    unused_keys.insert(k);
                } else {
                    closed.erase(b_it);
                    opened.insert(k);
                }
            }
            for (int cbox : containedBoxes[box]) {
                if (status[cbox]) {
                    opened.insert(cbox);
                } else {
                    auto k_it = unused_keys.find(cbox);
                    if (k_it == unused_keys.end()) {
                        closed.insert(cbox);
                    } else {
                        unused_keys.erase(k_it);
                        opened.insert(cbox);
                    }
                }
            }
        }
        return total_candies;
    }
};
 