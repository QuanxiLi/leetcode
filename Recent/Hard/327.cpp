
a#include <vector>
#include <stack>
#include <algorithm>

using namespace std;

class Solution {
	struct Node {
		short left, right;
		short size;
		short height;
		int64_t value;
	};

	class BinaryTree {
		Node *nodes;
		int count;
		// root: nodes[0]

		short getSize(int idx) const {
			return idx >= 0 ? nodes[idx].size : 0;
		}

		short getHeight(int idx) const {
			return idx >= 0 ? nodes[idx].height : 0;
		}

		void llrot(Node *node) {
			int left = node->left;
			int right = node->right;
			int ll = nodes[left].left;
			int lr = nodes[left].right;
			node->left = ll;
			node->right = left;
			swap(node->value, nodes[left].value);
			nodes[left].left = lr;
			nodes[left].right = right;
			nodes[left].size = getSize(lr) + getSize(right) + 1;
			nodes[left].height = max(getHeight(lr), getHeight(right)) + 1;
			node->height = max(nodes[left].height, getHeight(right)) + 1;
		}
		void rrrot(Node *node) {
			int left = node->left;
			int right = node->right;
			int rl = nodes[right].left;
			int rr = nodes[right].right;
			node->left = right;
			node->right = rr;
			swap(node->value, nodes[right].value);
			nodes[right].left = left;
			nodes[right].right = rl;
			nodes[right].size = getSize(rl) + getSize(left) + 1;
			nodes[right].height = max(getHeight(rl), getHeight(left)) + 1;
			node->height = max(getHeight(left), nodes[right].height) + 1;
		}
		void lrrot(Node *node) {
			rrrot(nodes + node->left);
			llrot(node);
		}
		void rlrot(Node *node) {
			llrot(nodes + node->right);
			rrrot(node);
		}

		// return: height after insertion
		void insert(Node *parent, int64_t x) {
			if (parent->value >= x) {
				if (parent->left < 0) {
					parent->left = count;
				} else {
					insert(nodes + parent->left, x);
				}
				int lh = nodes[parent->left].height;
				int rh = parent->right >= 0 ? nodes[parent->right].height : 0;
				if (lh - rh > 1) {
					if (x <= nodes[parent->left].value) {
						llrot(parent);
					} else {
						lrrot(parent);
					}
				} else {
					parent->height = max(lh, rh) + 1;
				}
			} else {
				if (parent->right < 0) {
					parent->right = count;
				} else {
					insert(nodes + parent->right, x);
				}
				int rh = nodes[parent->right].height;
				int lh = parent->left >= 0 ? nodes[parent->left].height : 0;
				if (rh - lh > 1) {
					if (x <= nodes[parent->right].value) {
						rlrot(parent);
					} else {
						rrrot(parent);
					}
				} else {
					parent->height = max(lh, rh) + 1;
				}
			}
			parent->size++;
		}
	public:
		BinaryTree(int max_count) : count(1) {
			nodes = new Node[max_count];
			nodes[0].value = 0;
			nodes[0].left = -1;
			nodes[0].right = -1;
			nodes[0].size = 1;
			nodes[0].height = 1;
		}
		~BinaryTree() {
			delete[] nodes;
		}
		void insert(int64_t x) {
			nodes[count].value = x;
			nodes[count].left = -1;
			nodes[count].right = -1;
			nodes[count].size = 1;
			nodes[count].height = 1;
			insert(nodes, x);
			count++;
		}
		int countLessEqualThan(int64_t upper_bound) {
			int idx = 0;
			int count = 0;
			while (idx >= 0) {
				if (nodes[idx].value <= upper_bound) {
					count++;
					if (nodes[idx].left >= 0) {
						count += nodes[nodes[idx].left].size;
					}
					idx = nodes[idx].right;
				} else {
					idx = nodes[idx].left;
				}
			}
			return count;
		}
	};

public:
	int countRangeSum(vector<int>& nums, int lower, int upper) {
		int64_t sum = 0;
		int count = 0;
		BinaryTree tree(nums.size() + 1);
		for (int x : nums) {
			sum += x;
			int64_t upper_sum = sum - lower;
			int64_t lower_sum = sum - upper;
			count += tree.countLessEqualThan(upper_sum)
				- tree.countLessEqualThan(lower_sum - 1);
			tree.insert(sum);
		}
		return count;
	}
};

