#include <unordered_map>

class Solution {
public:
    int distinctEchoSubstrings(string text) {
        int n = text.size();
        int *equal_mark = new int[n];
        int *equal_mark2 = new int[n];
        memset(equal_mark, 0, n * sizeof(int));
        memset(equal_mark2, 0, n * sizeof(int));

        int count = 0;
        for (int len = 1; len <= n / 2; len++) {
            unordered_map<int, int> hash2index;
            for (int i = 0; i <= n - len; i++) {
                int hash_value = (equal_mark2[i] << 8) | text[i + len - 1];
                auto it = hash2index.find(hash_value);
                if (it != hash2index.end()) {
                    int idx = it->second;
                    equal_mark[i] = idx;
                } else {
                    hash2index[hash_value] = i;
                    equal_mark[i] = i;
                }
            }
            for (int i = 0; i <= n - len * 2; i++) {
                int eqm = equal_mark[i];
                if (equal_mark[i + len] == eqm && equal_mark2[eqm] >= 0) {
                    count++;
                    equal_mark2[eqm] = -1;
                }
            }
            swap(equal_mark, equal_mark2);
        }
        delete [] equal_mark;
        delete [] equal_mark2;
        return count;
    }
};
