#include <string>

using namespace std;

class Solution {
public:
    string getPermutation(int n, int k) {
        int *factorial = new int[n + 1];
        bool *used = new bool[n];
        for (int i = 0; i < n; i++) {
            used[i] = false;
        }
        factorial[0] = 1;
        int fact_ = 1;
        for (int i = 1; i <= n; i++) {
            fact_ *= i;
            factorial[i] = fact_;
        }
        string result;
        result.reserve(n);
        for (int i = n - 1; i >= 0; i--) {
            // i + 1 digits left
            int f = factorial[i];
            int x = k / f;
            k = k % f;
            if (k == 0) {
                k = f;
                x--;
            }
            for(int i = 0; i < n; i++) {
                if (used[i]) {
                    continue;
                }
                if (x == 0) {
                    used[i] = true;
                    result.push_back('1' + i);
                    break;
                }
                x--;
            }
        }
        delete[] used;
        delete[] factorial;
        return result;
    }
};
