#include <vector>
#include <algorithm>
#include <string>

using namespace std;

class Solution {
public:
	vector<int> movesToStamp(string stamp, string target) {
		int n = target.size();
		int m = stamp.size();
		if (n < m) { return {}; }
		vector<int> result;

		auto find = [&target, &stamp, m, n](int from) {
			for (int i = from; i <= n - m; i++) {
				for (int j = 0; j < m; j++) {
					if (target[i + j] != stamp[j])
						goto F_NOT_MATCH;
				}
				return i;
			F_NOT_MATCH:
				continue;
			}
			return -1;
		};

		auto find_start = [&target, &stamp, m, n](int from, int to) {
			for (int i = to - 1; i >= from; i--) {
				if (to - i >= m) { return -1; }
				for (int j = 0; j < to - i; j++) {
					if (target[i + j] != stamp[j])
						goto FS_NOT_MATCH;
				}
				return i;
			FS_NOT_MATCH:
				continue;
			}
			if (from > 0) {
				for (int i = 1; i < m; i++) {
					for (int j = from; j < to; j++) {
						if (target[j] != stamp[i + j - from])
							goto FS2_NOT_MATCH;
					}
					return from - i;
				FS2_NOT_MATCH:
					continue;
				}
			}
			return -1;
		};

		auto find_end = [&target, &stamp, m, n](int from) {
			for (int i = 1; i <= m; i++) {
				if (from + i > n) { return n + 1; }
				int t_beg = from + i - m;
				for (int j = m - i; j < m; j++) {
					if (target[t_beg + j] != stamp[j]) {
						goto FE_NOT_MATCH;
					}
				}
				return from + i;
			FE_NOT_MATCH:
				continue;
			}
			return n + 1;
		};

		int left = 0;
		while (left < n) {
			int x = find(left);
			if (x < 0) { return {}; }
			result.push_back(x);
			int y = x + m;
			while (x > left) {
				x = find_start(left, x);
				if (x < 0) { return {}; }
				result.push_back(x);
			}
			while (true) {
				left = y;
				y = find_end(y);
				if (y > n) { break; }
				result.push_back(y - m);
			}
		}

		// reverse result
		int i = 0;
		int j = result.size() - 1;
		while (i < j) {
			swap(result[i], result[j]);
			i++;
			j--;
		}
		return result;
	}
};
