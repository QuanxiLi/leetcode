#include <vector>
#include <string>
#include <algorithm>

using namespace std;

class Solution {
public:
    int minDeletionSize(vector<string>& A) {
        int n = A[0].size();
        int *depth = new int[n];
        int max_depth = 0;
        for (int i = 0; i < n; i++) {
            int p = -1;
            int d = 0;
            for (int j = i - 1; j >= 0; j--) {
                if (depth[j] < d) { continue; }
                for (auto & s : A) {
                    if (s[j] > s[i])
                        goto NEXT_J;
                }
                d = depth[j];
                p = j;
            NEXT_J:
                continue;
            }
            depth[i] = d + 1;
            max_depth = max(max_depth, depth[i]);
        }
        delete [] depth;
        return n - max_depth;
    }
};   
