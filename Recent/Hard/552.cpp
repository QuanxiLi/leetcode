#include <stack>
#include <algorithm>

using namespace std;

constexpr unsigned N = 1000000007;

class Solution {
public:
	int checkRecord(int n) {
		// x, lx, llx
		// x, xl, xll
		// x, a
		unsigned state_1[18] = {
			1,  // x x x
			0,  // lx x x
			0,  // llx x x
			0,  // x xl x
			1,  // lx xl x
			0,  // llx xl x
			0,  // x xll x
			0,  // lx xll x
			0,  // llx xll x
			1,  // x x a
			0, 0, 0, 0, 0, 0, 0, 0
		};
		unsigned state_2[18] = {
			1, 1, 0, 1, 0, 0, 0, 0, 1, 2, 1, 0, 1, 0, 0, 0, 0, 0
		};
		unsigned state_a[18];
		unsigned state_b[18];
		unsigned *state = state_a;
		unsigned *new_state = state_b;

		stack<int> stk;
		int m = n;
		while (m > 1) {
			stk.push(m);
			m /= 2;
		}

		memcpy(state, state_1, sizeof(state_1));
		while (!stk.empty()) {
			m = stk.top();
			stk.pop();
			if (m / 2 == 1) {
				memcpy(state, state_2, sizeof(state_2));
			}
			else {
				for (int left = 0; left < 3; left++) {
					for (int right = 0; right < 3; right++) {
						int k = left + right * 3;
						uint64_t count = 0;
						for (int r = 0; r < 3; r++) {
							for (int l = 0; l <= 2 - r; l++) {
								count += static_cast<uint64_t>(state[left + r * 3]) * state[l + right * 3];
								count %= N;
							}
						}
						new_state[k] = count;
						k += 9;
						count = 0;
						for (int r = 0; r < 3; r++) {
							for (int l = 0; l <= 2 - r; l++) {
								count += static_cast<uint64_t>(state[left + r * 3 + 9])
                                    * state[l + right * 3];
								count += static_cast<uint64_t>(state[left + r * 3])
                                    * state[l + right * 3 + 9];
								count %= N;
							}
						}
						new_state[k] = count;
					}
				}
				swap(state, new_state);
			}
			if (m % 2) {
				for (int left = 0; left < 3; left++) {
					int count = 0;
					for (int r = 0; r < 3; r++) {
						count += state[left + r * 3];
					}
					count %= N;
					new_state[left] = count;
					new_state[left + 3] = state[left];
					new_state[left + 6] = state[left + 3];
					for (int r = 0; r < 3; r++) {
						count += state[left + r * 3 + 9];
					}
					count %= N;
					new_state[left + 9] = count;
					new_state[left + 12] = state[left + 9];
					new_state[left + 15] = state[left + 12];
				}
				swap(state, new_state);
			}
		}
		int result = 0;
		for (int i = 0; i < 18; i++) {
			result = (result + state[i]) % N;
		}
		return result;
	}
};
