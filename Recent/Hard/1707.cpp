#include <vector>
#include <algorithm>

class Solution {
    // 寻找nums[i...j]中不超过x的最大值位置
    static int find_no_greater_than(const vector<int> &nums, int i, int j, int x) {
        while (i < j) {
            int mid = (i + j + 1) / 2;
            if (nums[mid] > x) {
                j = mid - 1;
            } else {
                i = mid;
            }
        }
        return j;
    }

    // 寻找nums[i...j]中不小于x的最小值位置
    static int find_no_less_than(const vector<int> &nums, int i, int j, int x) {
        while (i < j) {
            int mid = (i + j) / 2;
            if (nums[mid] < x) {
                i = mid + 1;
            } else {
                j = mid;
            }
        }
        return i;
    }

public:
    vector<int> maximizeXor(vector<int>& nums, vector<vector<int>>& queries) {
        sort(nums.begin(), nums.end());
        vector<int> results;
        results.reserve(queries.size());
        for (auto &q : queries) {
            int x = q[0];
            int m = q[1];
            if (nums[0] > m) {
                results.push_back(-1);
            } else {
                int i = 0;
                int j = nums.size() - 1;
                j = find_no_greater_than(nums, i, j, m);
                for (int mask = 1 << 30; mask > 0; mask >>= 1) {
                    int xmask = mask & (mask ^ x);
                    if (xmask) {
                        // 尽量寻找对应位为1的
                        if (nums[j] & mask) {
                            i = find_no_less_than(nums, i, j, nums[j] & ~(mask - 1));
                        }
                    } else {
                        // 尽量寻找对应位为0的
                        if ((nums[i] & mask) == 0) {
                            j = find_no_greater_than(nums, i, j, nums[i] | (mask - 1));
                        }
                    }
                }
                assert(nums[i] == nums[j]);
                results.push_back(nums[i] ^ x);
            }
        }
        return results;
    }
};
