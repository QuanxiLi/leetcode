class Solution {
public:
    int findKthNumber(int m, int n, int k) {
        auto getK = [m, n](int x) {
            int sum = 0;
            for (int i = 1; i <= m; i++) {
                sum += min(n, x / i);
            }
            return sum;
        };
        int lower = 1;
        int upper = m * n;
        while (upper > lower) {
            int mid = (lower + upper) / 2;
            if (getK(mid) < k) {
                lower = mid + 1;
            } else {
                upper = mid;
            }
        }
        return lower;
    }
};
