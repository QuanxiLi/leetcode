#include <string>

using namespace std;

constexpr int N = 1000000007;

class Solution {
public:
    int distinctSubseqII(string S) {
        int dp[26] = { 0 };
        int result = 1;
        for (int i = 0; i < S.size(); i++) {
            int c = S[i] - 'a';
            int new_result = result * 2 - dp[c];
            dp[c] = result;
            result = new_result;
            if (result < 0)
                result += N;
            else if (result >= N)
                result -= N;
            if (dp[c] >= N)
                dp[c] -= N;
        }
        return result == 0 ? N - 1 : result - 1;
    }
};
