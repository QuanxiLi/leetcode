#include <unordered_map>
#include <map>
#include <vector>
#include <algorithm>

using namespace std;

class Solution {
public:
	int minOperations(vector<int>& target, vector<int>& arr) {
		int m = target.size();
		int n = arr.size();
		unordered_map<int, int> target_index;
		for (int i = 0; i < m; i++) {
			target_index[target[i]] = i;
		}
		// first...last 之间的 save 值都为 value
		// last = 下一个键 - 1
		// i 的 save 值含义是，得到 target[0...i] 需要 i - save 步操作
		map<int/*first*/, int/*value*/, greater<int>> save;
		// save 值随下标（非严格）上升
		save[-1] = 0;

		for (int i = 0; i < n; i++) {
			auto it = target_index.find(arr[i]);
			if (it == target_index.end())
				continue;
			int idx = it->second;
			// 如果 idx 的 save 值与 idx - 1 的相同，就将idx（和其后同值的）+ 1
			auto lb = save.lower_bound(idx);
			if (lb->first < idx) {
				int save_value = lb->second + 1;
				// 如果下一个值等于 save_value ，应该合并它们
				if (lb != save.begin()) {
					lb--;
					if (lb->second == save_value)
						save.erase(lb);
				}
				save[idx] = save_value;
			}
		}
		return m - save.begin()->second;
	}
};
