#include <vector>

using namespace std;

class Solution {
	struct State {
		int count_sum;
		int count[9];
		State() : count_sum(0) {
			memset(count, 0, sizeof(count));
		}
		State(const State &other) {
			memcpy(this, &other, sizeof(State));
		}
		bool operator < (const State &other) const {
			if (count_sum == other.count_sum) {
				for (int i = 8; i >= 0; i--) {
					if (count[i] != other.count[i])
						return count[i] < other.count[i];
				}
				return false;
			} else {
				return count_sum < other.count_sum;
			}
		}
	};
public:
	string largestNumber(vector<int>& cost, int target) {
		State *state = new State[target];
		for (int all_cost = 1; all_cost <= target; all_cost++) {
			for (int j = 0; j < 9; j++) {
				if (cost[j] < all_cost && state[all_cost - cost[j] - 1].count_sum > 0) {
					State new_state = state[all_cost - cost[j] - 1];
					new_state.count_sum++;
					new_state.count[j]++;
					if (state[all_cost - 1] < new_state) {
						state[all_cost - 1] = new_state;
					}
				} else if (cost[j] == all_cost) {
					if (state[all_cost - 1].count_sum <= 1) {
						State new_state;
						new_state.count_sum = 1;
						new_state.count[j] = 1;
						if (state[all_cost - 1] < new_state) {
							state[all_cost - 1] = new_state;
						}
					}
				}
			}
		}
		State &final_state = state[target - 1];
		string result;
		if (final_state.count_sum == 0) {
			result = "0";
		} else {
			for (int i = 9; i > 0; i--) {
				for (int j = final_state.count[i - 1]; j > 0; j--) {
					result.push_back('0' + i);
				}
			}
		}
		delete[] state;
		return result;
	}
};
