#include <vector>
#include <algorithm>

using namespace std;

class Solution {
public:
	int minMoves(vector<int>& nums, int k) {
		int n = nums.size();
		int left = 0;   // 要移动的最左侧的1的位置
		int right;      // 要移动的最右侧的1的位置
		while (!nums[left]) left++;
		right = left;
		int pos = left;
		int cost = 0;
		for (int i = 1; i < k;) {
			right++;
			if (nums[right]) {
				cost += right - pos - i;
				i++;
			}
		}
		int left1 = 0;  // left到pos之间的1
		int right1 = k; // pos到right之间的1
		int min_cost = cost;
		int next1 = right + 1;
		while (next1 < n && !nums[next1])    next1++;
		// 初始化完成
		while (pos < n) {
			if (nums[pos]) {
				left1++;
				right1--;
			}
			else {
				cost += left1 - right1;
			}
			pos++;
			while (left1 > 0 && next1 < n) {
				int left0 = pos - left - left1;
				int next_right0 = next1 - right1 - pos;
				int new_cost = cost - left0 + next_right0;
				if (new_cost < cost) {
					cost = new_cost;
					left1--;
					right1++;
					right = next1;
					left++;
					while (!nums[left])   left++;
					next1++;
					while (next1 < n && !nums[next1])    next1++;
				} else {
                    break;
                }
			}
			min_cost = min(cost, min_cost);
		}
		return min_cost;
	}
};
