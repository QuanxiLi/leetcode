class Solution {
public:
    vector<vector<int>> criticalConnections(int n, vector<vector<int>>& connections) {
        vector<int> topo_index(n, -1);
        vector<int> parent(n, -1);
        vector<int> alias(n, -1);
        topo_index[0] = 0;
        alias[0] = 0;
        int current_index = 1;
        vector<deque<int>> edges(n - 1);
        unordered_set<int> key_nodes;
        for (auto & conn : connections) {
            int a = conn[0];
            int b = conn[1];
            if (a > b) { swap(a, b); }
            edges[a].push_back(b);
        }
        for (int i = 0; i < n - 1; i++) {
            for (auto j : edges[i]) {
                if (parent[j] == -1) {
                    parent[j] = i;
                    topo_index[j] = current_index++;
                    alias[j] = j;
                    key_nodes.insert(j);
                } else {    // 合并 i, j 到其公共父节点之间的所有点
                    int a = i, b = j;
                    while (a != alias[a]) { a = alias[a]; }
                    while (b != alias[b]) { b = alias[b]; }
                    while (a != b) {
                        if (topo_index[a] > topo_index[b]) {
                            a = parent[a];
                            while (a != alias[a]) { a = alias[a]; }
                        } else {
                            b = parent[b];
                            while (b != alias[b]) { b = alias[b]; }
                        }
                    }
                    int x = a;
                    a = i;
                    b = j;
                    while (a != x) {
                        int t = a;
                        if (alias[a] == a) {
                            key_nodes.erase(a);
                            a = parent[a];
                        } else {
                            a = alias[a];
                        }
                        alias[t] = x;
                    }
                    while (b != x) {
                        int t = b;
                        if (alias[b] == b) {
                            key_nodes.erase(b);
                            b = parent[b];
                        } else {
                            b = alias[b];
                        }
                        alias[t] = x;
                    }
                    
                }
            }
        }
        vector<vector<int>> result;
        for (auto kn : key_nodes) {
            result.push_back({ kn, parent[kn] });
        }
        return result;
    }
};
