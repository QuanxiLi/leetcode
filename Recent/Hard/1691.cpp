#include <vector>
#include <algorithm>
#include <tuple>

using namespace std;

class Solution {
public:
    int maxHeight(vector<vector<int>>& cuboids) {
        int n = cuboids.size();
        auto arr = new tuple<unsigned char, unsigned char, unsigned char>[n];
        for (int i = 0; i < n; i++) {
            int a = cuboids[i][0];
            int b = cuboids[i][1];
            int c = cuboids[i][2];
            if (a > b) { swap(a, b); }
            if (b > c) { swap(b, c); }
            if (a > b) { swap(a, b); }
            arr[i] = make_tuple(a, b, c);
        }
        sort(arr, arr + n);
        auto maxh = new tuple<int, int, int>[n];
        int result = 0;
        for (int i = n - 1; i >= 0; i--) {
            auto get_maxh = [arr, maxh, i, n, &result](int x, int y, int z) {
                int m = 0;
                for (int j = i + 1; j < n; j++) {
                    auto [a, b, c] = arr[j];
                    if (x <= b && y <= c && z <= a) {
                        m = max(m, get<0>(maxh[j]));
                    }
                    if (x <= a && y <= c && z <= b) {
                        m = max(m, get<1>(maxh[j]));
                    }
                    if (x <= a && y <= b && z <= c) {
                        m = max(m, get<2>(maxh[j]));
                    }
                }
                m += z;
                result = max(m, result);
                return m;
            };
            auto [a, b, c] = arr[i];
            maxh[i] = make_tuple(
                get_maxh(b, c, a),
                get_maxh(a, c, b),
                get_maxh(a, b, c)
            );
        }

        delete [] maxh;        
        delete [] arr;
        return result;
    }
};
