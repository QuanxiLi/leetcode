#include <vector>
#include <algorithm>

using namespace std;

class Solution {
public:
    int minDifficulty(vector<int>& jobDifficulty, int d) {
        int n = jobDifficulty.size();
        if (n < d) {
            return -1;
        }
        // opt(n, d) = min{opt(n - k, d - 1) + max(dif(n - k : n)) | k = 1...n - d - 1}
        int *opt = new int[n * d];
        int *maxdiff = new int[n];

        for (int m = 0; m < n; m++) {
            maxdiff[m] = jobDifficulty[m];
            for (int i = m - 1; i >= 0; i--) {
                maxdiff[i] = max(maxdiff[i + 1], jobDifficulty[i]);
            }

            opt[m * d] = maxdiff[0];    // date = 0
            for (int date = 1; date < d && date <= m; date++) {
                int o = INT_MAX;
                for (int p = date - 1; p <= m - 1; p++) {
                    o = min(o, opt[p * d + date - 1] + maxdiff[p + 1]);
                }
                opt[m * d + date] = o;
            }
        }

        int result = opt[n * d - 1];

        delete [] opt;
        delete [] maxdiff;

        return result;
    }
};
