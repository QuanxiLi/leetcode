#include <tuple>
#include <vector>
#include <algorithm>

using namespace std;

class Solution {
public:
    int maxEnvelopes(vector<vector<int>>& envelopes) {
        int n = envelopes.size();
        if (n == 0) return 0;
        vector<tuple<int, int>> envs;
        envs.reserve(n);
        for (auto &e : envelopes) {
            envs.emplace_back(e[0], e[1]);
        }
        sort(envs.begin(), envs.end());
        vector<tuple<int, int>> sections;
        int *count = new int[n];
        int best = 0;
        int current_width = get<0>(envs[n - 1]);
        int section_end = n - 1;
        for (int i = n - 1; i >= 0; i--) {
            auto [w, h] = envs[i];
            if (current_width != w) {
                sections.emplace_back(i + 1, section_end);
                section_end = i;
                current_width = w;
            }
            int c = 0;
            for (auto [s, e] : sections) {
                for (int k = s; k <= e; k++) {
                    if (get<1>(envs[k]) > h) {
                        c = max(c, count[k]);
                        break;
                    }
                }
            }
            count[i] = c + 1;
            best = max(best, count[i]);
        }
        delete [] count;
        return best;
    }
};
