#include <vector>
#include <unordered_map>

using namespace std;

class Solution {
public:
    int numberOfArithmeticSlices(vector<int>& A) {
        int n = A.size();
        unordered_multimap<int, int> pos;
        int count_all = 0;
        auto arrs = new unordered_map<int, int>[n];
        for (int i = 0; i < n; i++) {
            int64_t x = A[i];
            for (int j = 1; j < i; j++) {
                int64_t delta = x - A[j];
                int64_t y = A[j] - delta;
                if (y < INT_MIN || y > INT_MAX)
                    continue;
                int count_j = 0;
                auto [from, to] = pos.equal_range(y);
                for (auto it = from; it != to; it++) {
                    if(it->second < j) {
                        count_j++;
                    }
                }
                for (auto [d, c] : arrs[j]) {
                    if (d == delta) {
                        count_j += c;
                    }
                }
                if (count_j != 0) {
                    count_all += count_j;
                    auto it = arrs[i].find(delta);
                    if (it == arrs[i].end()) {
                        arrs[i][delta] = count_j;
                    } else {
                        it->second += count_j;
                    }
                }
            }
            pos.emplace(x, i);
        }
        return count_all;
    }
};
