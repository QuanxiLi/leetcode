#include <vector>
#include <cstring>
#include <algorithm>

using namespace std;

class Solution {
public:
    int minTaps(int n, vector<int>& ranges) {
        int *right_range = new int[n + 1];
        memset(right_range, 0, sizeof(int) * (n + 1));
        for (int i = 0; i <= n; i++) {
            int left = max(0, i - ranges[i]);
            int right = i + ranges[i];
            right_range[left] = max(right_range[left], right);
        }
        int count = 0;
        int rr = 0;
        for (int i = 0; i < n; i++) {
            rr = max(right_range[i], rr);
            if (rr <= i) {
                delete [] right_range;
                return -1;
            }
            right_range[i] = rr;
        }
        int tap = 0;
        for (int i = 0; i < n; i = right_range[i]) {
            tap++;
        }
        delete [] right_range;
        return tap;
    }
};
