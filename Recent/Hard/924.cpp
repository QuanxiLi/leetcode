#include <vector>
#include <algorithm>
#include <map>
#include <unordered_map>

using namespace std;

class Solution {
public:
	int minMalwareSpread(vector<vector<int>>& graph, vector<int>& initial) {
		int n = graph.size();
		int *parent = new int[n];
		unordered_map<int, int> size;

		auto get_root = [&](int a) {
			int r = a;
			while (r != parent[r]) {
				r = parent[r];
			}
			while (a != parent[a]) {
				int t = a;
				a = parent[a];
				parent[t] = r;
			}
			return r;
		};

		auto make_union = [&](int a, int b) {
			a = get_root(a);
			while (b != a) {
				int t = b;
				b = parent[b];
                if (b == t) { size[a] += size[b]; }
				parent[t] = a;
			}
		};

		for (int x : initial) {
			parent[x] = -1;
		}
		for (int i = 0; i < n; i++) {
			if (parent[i] == -1)
				continue;
			parent[i] = i;
			size[i] = 1;
			auto &graph_i = graph[i];
			for (int j = 0; j < i; j++) {
				if (graph_i[j] == 1 && parent[j] != -1) {
					make_union(i, j);
				}
			}
		}

		unordered_map<int, int> infected_by;
		map<int, int> remove_benifit;
		for (int x : initial) {
			auto &graph_x = graph[x];
			int benefit = 1;
			for (int i = 0; i < n; i++) {
				if (graph_x[i] == 0 || i == x)
					continue;
				if (parent[i] == -1)
					goto SHOULD_NOT_REMOVE;
				int r = get_root(i);
				auto it = infected_by.find(r);
				if (it != infected_by.end()) {
					if (it->second != x) {
						remove_benifit.erase(it->second);
						goto SHOULD_NOT_REMOVE;
					}
				}
				else {
					infected_by[r] = x;
					benefit += size[r];
				}
			}
			remove_benifit[x] = benefit;
		SHOULD_NOT_REMOVE:
			continue;
		}

		delete[] parent;

		int max_benefit = 0;
		int result;
		for (auto[i, b] : remove_benifit) {
			if (b > max_benefit) {
				result = i;
				max_benefit = b;
			}
		}
		if (max_benefit == 0) {
			result = n;
			for (int i : initial) {
				result = min(result, i);
			}
		}
		return result;
	}
};
