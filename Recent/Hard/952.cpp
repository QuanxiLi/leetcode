#include <vector>
#include <unordered_map>
#include <algorithm>

using namespace std;

const int primes[] = { 2, 3, 5, 7, 11, 13, 17, 19, 23, 29, 31, 37, 41, 43, 47, 53, 59, 61, 67, 71, 73, 79, 83, 89, 97, 101, 103, 107, 109, 113, 127, 131, 137, 139, 149, 151, 157, 163, 167, 173, 179, 181, 191, 193, 197, 199, 211, 223, 227, 229, 233, 239, 241, 251, 257, 263, 269, 271, 277, 281, 283, 293, 307, 311, 313 };

class Solution {
public:
    int largestComponentSize(vector<int>& A) {
        if (A.empty()) { return 0; }
        unordered_map<int, int> parent;
        unordered_map<int, int/* size */> roots;

        auto get_root = [&](int x) {
            auto it = parent.find(x);
            if (it == parent.end()) {
                parent[x] = x;
                roots[x] = 0;
                return x;
            }
            int y = it->second;
            int x0 = x;
            while (x != y) {
                x = y;
                y = parent[x];
            }
            int r = y;
            y = parent[x];
            x = x0;
            while (x != y) {
                parent[x] = r;
                x = y;
                y = parent[x];
            }
            return r;
        };

        auto merge = [&](int r1, int r2) {
            parent[r1] = r2;
            roots[r2] += roots[r1];
            roots.erase(r1);
        };

        for (int x : A) {
            int root = 0;
            for (int p : primes) {
                if (x == 1) { break; }
                if (x % p == 0) {
                    int r = get_root(p);
                    if (root == 0) {
                        root = r;
                        roots[r]++;
                    } else if (r != root) {
                        merge(r, root);
                    }

                    do {x /= p; } while (x % p == 0);
                }
            }
            if (x != 1) {
                    int r = get_root(x);
                    if (root == 0) {
                        root = r;
                        roots[r]++;
                    } else if (r != root) {
                        merge(r, root);
                    }
            }
        }

        int result = 1;
        for (auto [r, s] : roots) {
            result = max(s, result);
        }
        return result;
    }
};
