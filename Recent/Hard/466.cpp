#include <string>
#include <unordered_map>
#include <set>
#include <algorithm>

using namespace std;

class Solution {
	// there is one `s2` in `s1[from]` to `s1[return value]`
	static inline int help(int from, const string &s1, const string &s2) {
		for (char c : s2) {
			for (int i = 0; i < s1.size(); i++) {
				if (s1[from % s1.size()] == c)
					goto FOUND;
				from++;
			}
			return -1;  // NOT FOUND
		FOUND:
			from++;
		}
		return from - 1;
	}
public:
	int getMaxRepetitions(string s1, int n1, string s2, int n2) {
		char s2_0 = s2[0];
		unordered_map<int, int> from_to;
		unordered_map<int, int> next_from;
		set<int> froms;
		set<int> nexts;
		for (int from = 0; from < s1.size(); from++) {
			if (s1[from] == s2_0) {
				int to = help(from, s1, s2);
				if (to < 0) { return 0; }
				from_to[from] = to;
				froms.insert(from);
			}
		}
		if (froms.empty()) {
			return 0;
		}
		for (auto[f, t] : from_to) {
			auto ub = froms.upper_bound(t % s1.size());
			int nxt;
			if (ub == froms.end()) {
				nxt = *(froms.begin());
			}
			else {
				nxt = *ub;
			}
			next_from[t] = nxt;
			nexts.insert(nxt);
		}
		// find all cycles
		while (true) {
			set<int> nexts_;
			for (auto f : nexts) {
				int nf = next_from[from_to[f]];
				nexts_.insert(nf);
			}
			if (nexts_.size() >= nexts.size())
				break;
			nexts = move(nexts_);
		}
		unordered_map<int, pair<int, int>> cycles;	// starts -> (m, n)
		while (!nexts.empty()) {
			int start = *nexts.begin();
			nexts.erase(start);
			int f = start;
			int n = 0;  // n * s1
			int m = 0;  // m * s2
			while (true) {
				int t = from_to[f];
				n += t / s1.size();
				f = next_from[t];
				if (f <= (t % s1.size())) n++;
				m++;
				if (f == start) {
					break;
				}
				else {
					nexts.erase(f);
				}
			}
			cycles[start] = make_pair(m, n);
		}

		// compute the final result
		int result = 0;
		for (auto f : froms) {
			int m = 0;
			int n1_last = n1;
			while (true) {
				if (n1_last > 1 && cycles.find(f) != cycles.end()) {
					auto[cm, cn] = cycles[f];
					int c = (n1_last - 1) / cn;
					n1_last -= c * cn;
					m += c * cm;
				}
				int t = from_to[f];
				n1_last -= t / s1.size();
				if (n1_last <= 0) {
					result = max(result, m);
					break;
				}
				f = next_from[t];
				if (f <= (t % s1.size())) n1_last--;
				m++;
			}
		}
		return result / n2;
	}
};

