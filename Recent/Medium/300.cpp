#include <vector>
#include <map>
#include <algorithm>

using namespace std;

class Solution {
public:
    int lengthOfLIS(vector<int>& nums) {
        map<int, int, greater<int>> records;
        int best = 0;
        for (int x : nums) {
            auto it = records.upper_bound(x);
            int r;
            if (it == records.end()) {
                r = 1;
            } else {
                r = it->second + 1;
                if (it != records.begin()) {
                    it--;
                    while (true) {
                        auto erase_it = it;
                        if (erase_it->second <= r) {
                            if (it != records.begin()) {
                                it--;
                                records.erase(erase_it);
                            } else {
                                records.erase(erase_it);
                                break;
                            }
                        } else {
                            break;
                        }
                    }
                }
            }
            records[x] = r;
            best = max(best, r);
        }
        return best;
    }
};
